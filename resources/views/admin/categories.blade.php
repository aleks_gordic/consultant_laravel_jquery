@extends('layout.private')
@section('content')

<div class="wrapper member-sidebar">
    @include('elements.admin_sidebar')
    <div class="content-wrapper adminprof">
        <div class="content_holesecion">
		    <div class="page-list d-flex flex-column">
                <div class="pages-heading category-heading d-flex">
                    <h2 class="mr-auto mt-auto mb-auto">Categories</h2>
                    <a href="{{ route('create_category')}}"><button class="btn">Create category</button></a>
                </div>
		    </div>
			<div class="status-section consult-table cust-table table-responsive">
				<table class="table table-borderless" id="example">
                    <thead>
                        <tr class="top">
                            <th>CATEGORY</th>
							<th>DESCRIPTION</th>
							<th>SLUG</th>
							<th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($categories as $key => $data)
							<tr>
                                <td>{{$data->category_url}}</td>
                                <td>{{$data->category_description}}</td>
                                <td>{{$data->category_name}}</td>
                                <td><a style="display:block;line-height:22px;" href="{{ route('edit_category',['id' => $data->id])}}">Details </a></td>
						    </tr>
                        @endforeach
                    </tbody>
				</table>
			</div>
		</div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $(document).ready(function() {
		$('#example').DataTable();
    });
</script>
@endsection
