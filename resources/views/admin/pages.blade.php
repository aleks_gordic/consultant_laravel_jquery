@extends('layout.private')
@section('content')

<div class="wrapper member-sidebar">
    @include('elements.admin_sidebar')
    <div class="content-wrapper adminprof">
	    <div class="content_holesecion">
		    <div class="page-list d-flex flex-column">
                <div class="pages-heading d-flex">
                    <h2 class="mr-auto mt-auto mb-auto">Pages</h2>
                    <a href="{{ route('create_page')}}"><button class="btn">Create page</button></a>
                </div>
		    </div>
			<div class="status-section">
				<table class="table table-borderless" id="example">
                    <thead>
                        <tr class="top">
                        <th>Page Name</th>
                        <th>Status</th>
                        <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($pages as $key => $data)
                        <tr>
                            <td>{{$data->page_name}}</td>
                            <td><small>Published</small></td>
                            <td><a style="display:block;line-height:22px;" href="{{ route('edit_page',['id' => $data->id])}}">Details </a></td>
                        </tr>
                        @endforeach
                    </tbody>
				</table>
			</div>
		</div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
		$('#example').DataTable();
    });
</script>
@endsection
