@extends('layout.private')
@section('content')

<div class="wrapper member-sidebar">
    @include('elements.admin_sidebar')
    <div class="content-wrapper adminprof">
        <div class="content_holesecion">
		    <div class="page-list d-flex flex-column">
                <div class="pages-heading category-heading d-flex">
                    <h2 class="mr-auto mt-auto mb-auto">Consultants</h2>
                    <a href="{{ route('create_consultant')}}"><button class="btn">Create consultant</button></a>
                </div>
		    </div>
			<div class="status-section consult-table cust-table table-responsive">
				<table class="table table-borderless" id="example">
                    <thead>
                        <tr class="top">
                        <th>NUMBER</th>
                        <th>CONSULTANTS</th>
                        <th>INDUSTRY</th>
                        <th>REG. DATE</th>
                        <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($consultants as $key => $data)
                        @foreach($info as $key => $item)
                        <tr>
                            <td>{{$data->id}}</td>
                            <td><b>{{$data->first_name[0]}}{{$data->last_name[0]}}</b>{{$data->first_name}} {{$data->last_name}}</td>
                            @if($data->id == $item->unique_id)
                            <td>{{$item->industry_expertise}}</td>
                            @endif
                            <td>{{$data->created_at->format('d.m.Y')}}</td>
                            <td><a style="display:block;line-height:22px;" href="{{ route('edit_consultant',['id' => $data->id])}}" class="">Details </a></td>
                        </tr>
                        @endforeach
                        @endforeach
                    </tbody>
				</table>
			</div>
		</div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $(document).ready(function() {
		$('#example').DataTable();
    });
</script>
@endsection
