@extends('layout.private')
@section('content')

<div class="wrapper member-sidebar">
    @include('elements.admin_sidebar')
    <div class="content-wrapper adminprof">
        <div class="content_holesecion">
		    <div class="page-list d-flex flex-column">
                <div class="pages-heading category-heading d-flex">
                    <h2 class="mr-auto mt-auto mb-auto">Customers</h2>
                    <a href="{{ route('create_customer')}}"><button class="btn">Create customer</button></a>
                </div>
		    </div>
			<div class="status-section consult-table cust-table table-responsive">
				<table class="table table-borderless" id="example">
                    <thead>
                        <tr class="top">
                        <th>NUMBER</th>
                        <th>CUSTOMER</th>
                        <th>REG. DATE</th>
                        <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($customers as $key => $data)
                        <tr>
                            <td>{{$data->id}}</td>
                            <td><b>{{$data->first_name[0]}}{{$data->last_name[0]}}</b>{{$data->first_name}} {{$data->last_name}}</td>
                            <td>{{$data->created_at->format('d.m.Y')}}</td>
                            <td><a style="display:block;line-height:22px;" href="{{ route('edit_customer',['id' => $data->id])}}" class="">Details </a></td>
                        </tr>
                        @endforeach
                    </tbody>
				</table>
			</div>
		</div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $(document).ready(function() {
		$('#example').DataTable();
    });
</script>
@endsection
