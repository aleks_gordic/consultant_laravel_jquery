@extends('layout.private')
@section('content')

<div class="wrapper member-sidebar">
    @include('elements.admin_sidebar')
    <div class="content-wrapper adminprof">
        <div class="content_holesecion">
		    <div class="single-page d-flex flex-column">
			    <div class="single-page-heading single-page d-flex flex-column">
			        <a href="{{ route('consultantlist')}}">
                        <img src="{{ asset('images/back-icon.png')}}" alt="icon"/>
                    </a>
                </div>
                <div class="profile-uploader d-flex flex-column">
                    <form method="post" id="upload_form" enctype="multipart/form-data">
                        <h2>PROFILE IMAGE</h2>
                        <div class="profile-sec d-flex flex-column">
                            <div class="imageupload">
                                {{ csrf_field() }}
                                <input type="hidden" id="hidden_id" name="hidden_id">
                                <input type="hidden" id="checkbox_value" name="checkbox_value" value=5>
                                <div class="file-tab">
                                    <label class="btn btn-default btn-file">
                                        <span>Browse Photo</span>
                                        <input type="file" id="select_file" class="select_file" name="select_file">
                                    </label>
                                </div>
                            </div>
                            <a class="remove-btn btn" id="remove_photo">Remove image</a>
                            <label class="switch">
                                <input type="checkbox" id="image_access" value="1">
                                <span class="slider"></span>
                                <span class="uncheck"></span>
                            </label>
                            <input type="submit" name="upload" id="upload" class="sp-f cs btn save-btn" value="Upload">
                            <div class="alert" id="message" style="display: none"></div>
                        </div>
                    </form>
                </div>
                <div class="page-setting d-flex flex-column">
                    <h2>PAGE SETTINGS</h2>
                    <div class="page-seting-content d-flex flex-column">
                        <label>First name</label>
                        <input type="text" id="first_name" class="first_name">
                        <div class="alert" id="first_name_error"></div>
                        <label>Last name</label>
                        <input type="text" id="last_name" class="last_name">
                        <div class="alert" id="last_name_error"></div>
                        <label>E-mail</label>
                        <input type="text" id="email" class="email">
                        <div class="alert" id="email_error"></div>
                        <label>Phone</label>
                        <input type="text" id="phone" class="phone">
                        <div class="alert" id="phone_error"></div>
                        <label>Industry Expertise</label>
                        <select id="industry_expertise" class="form-control" style="height: auto!important;">
                            @foreach($categories as $data)
                            <option value="{{$data->category_name}}">{{$data->category_name}}</option>
                            @endforeach 
                        </select>
                        <div class="alert" id="industry_expertise_error"></div>				
                        <button class="sp-f cs save-btn btn" id="profile_save">Save</button>
                    </div>
                </div>
                <div class="profile-uploader d-flex flex-column">
                    <h2>CONTACT SETTINGS</h2>
                    <div class="profile-sec contact-sec d-flex flex-column">
                        <label class="heading-t">Phone</label>
                        <label class="switch">
                            <input type="checkbox"  id="phone_checkbox" class="phone_checkbox" value='1'>
                            <span class="slider"></span>
                            <span class="uncheck"></span>
                        </label>
                        <label class="heading-t">Chat</label>
                        <label class="switch">
                            <input type="checkbox" id="chat_checkbox" class="chat_checkbox" value='1'>
                            <span class="slider"></span>
                            <span class="uncheck"></span>
                        </label>
                        <label class="heading-t">Video</label>
                        <label class="switch">
                            <input type="checkbox" id="video_checkbox" class="video_checkbox" value='1'>
                            <span class="slider"></span>
                            <span class="uncheck"></span>
                        </label>
                        <button class="sp-f cs btn save-btn" id="contact_save">Save</button>
                    </div>
                </div>
                <div class="page-setting d-flex flex-column">
                    <h2>INVOICE SETTINGS</h2>
                    <div class="page-seting-content d-flex flex-column">
                        <label>Company name</label>
                        <input type="text" id="company_name" class="company_name">
                        <div class="alert" id="company_name_error"></div>
                        <label>Invoice e-mail</label>
                        <input type="text" id="invoice_mail" class="invoice_mail">
                        <div class="alert" id="invoice_mail_error"></div>
                        <label>First name</label>
                        <input type="text" id="company_first_name" class="company_first_name">
                        <div class="alert" id="company_first_name_error"></div>
                        <label>Last name</label>
                        <input type="text" id="company_last_name" class="company_last_name">
                        <div class="alert" id="company_last_name_error"></div>
                        <label>Address</label>
                        <input type="text" id="address" class="address">
                        <div class="alert" id="address_error"></div>
                        <label>Zip code</label>
                        <input type="text" id="zip_code" class="zip_code">
                        <div class="alert" id="zip_code_error"></div>
                        <label>Zip place</label>
                        <input type="text" id="zip_place" class="zip_place">
                        <div class="alert" id="zip_place_error"></div>
                        <button class="sp-f cs save-btn btn" id="invoice_save">Save</button>
                    </div>
                </div>
                <div class="page-setting meta-info d-flex flex-column">
                    <h2>SET PASSWORD</h2>
                    <div class="page-seting-content d-flex flex-column">
                        <label>Password</label>
                        <input type="text" id="password" class="password">
                        <div class="alert" id="password_error"></div>         
                        <label>Confirm password</label>
                        <input type="text" id="confirm_password" class="confirm_password">
                        <div class="alert" id="confirm_password_error"></div>
                        <button class="sp-f cs save-btn btn" id="password_save">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $("#remove_photo").click(function(){
        $('.imageupload').imageupload();

        $('#imageupload-disable').on('click', function() {
            $('.imageupload').imageupload('disable');
            $(this).blur();
        });
        $('#imageupload-enable').on('click', function() {
            $('.imageupload').imageupload('enable');
            $(this).blur();
        });
        $('#imageupload-reset').on('click', function() {
            $('.imageupload').imageupload('reset');
            $(this).blur();
        });
    });
    $("#image_access").click(function(){
        if ($('#image_access').is(":checked")) {
            $("#checkbox_value").val(1);
        } else {
            $("#checkbox_value").val(0);     
        }
    });
    $('#upload_form').on('submit', function(event){
        event.preventDefault();    
        $.ajax({
            url: '/create_consultant',
            headers:  {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            data:new FormData(this),
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) { 
                $('#message').css('display', 'block');
                $('#message').html(data.message);
                $('#message').addClass(data.class_name);
                $('#uploaded_image').html(data.uploaded_image);
                $('#message').hide(3000);
            }
        });
    });
    $("#profile_save").click(function(){
        var profile = {
            first_name: $("#first_name").val(),
            last_name: $("#last_name").val(),
            email: $("#email").val(),
            phone: $("#phone").val(),
            industry_expertise: $("#industry_expertise").val(),
            type: 'profile'
        };
        $.ajax({
            url: '/create_consultant',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            data: profile,
            dataType: 'JSON',
            success: function (data) { 
                var status=JSON.stringify(data['status']);
                if(status=='false') {
                    $.each(data.errors,function(index,value){
                        $("#" + index + "_error").show();
                        $("#" + index + "_error").text(value[0]);
                    });
                } else {
                    var id=JSON.stringify(data['id']);
                    if(id!='') {
                        $("#hidden_id").val(id);
                        $("#profile_save").prop("disabled", true);
                        alert("Customer added successfully");
                    } 
                }
            }
        });
    });
    $("#contact_save").click(function (){
        var contact = {
            phone_contact: $('#phone_checkbox:checkbox:checked').length > 0 ? 1: 0,
            chat_contact: $('#chat_checkbox:checkbox:checked').length > 0 ? 1: 0,
            video_contact: $('#video_checkbox:checkbox:checked').length > 0 ? 1: 0,
            hidden_id: $("#hidden_id").val(),
            type: "contact"
        };
        if ($("#hidden_id").val() != '') {
            $.ajax({
                url: '/create_consultant',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                data: contact,
                dataType: 'JSON',
                success: function (data) {
                    alert("Updated successfully");
                }
            });
        } else {
            alert("Please save page setting first");
        }        
    });
    $("#invoice_save").click(function (){
        var invoice = {
            company_name: $("#company_name").val(),
            invoice_mail: $("#invoice_mail").val(),
            company_first_name: $("#company_first_name").val(),
            company_last_name: $("#company_last_name").val(),
            address: $("#address").val(),
            zip_code: $("#zip_code").val(),
            zip_place: $("#zip_place").val(),
            hidden_id: $("#hidden_id").val(),
            type: "invoice"
        };
        if ($("#hidden_id").val() != '') {
            $.ajax({
                url: '/create_consultant',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                data: invoice,
                dataType: 'JSON',
                success: function (data) {
                    var status=JSON.stringify(data['status']);
                    if(status=='false') {
                        $.each(data.errors,function(index,value){
                            $("#" + index + "_error").show();
                            $("#" + index + "_error").text(value[0]);
                        });
                    } else {
                        $("#company_name_error").hide();
                        $("#invoice_mail_error").hide();
                        $("#company_first_name_error").hide();
                        $("#company_last_name_error").hide();
                        $("#address_error").hide();
                        $("#zip_code_error").hide();
                        $("#zip_place_error").hide();
                    }
                }
            });
        } else {
            alert("Please save page setting first");
        }
        
    });
    $("#password_save").click(function(){
        var password_info = {
            confirm_password: $("#confirm_password").val(),
            old_password: $("#old_password").val(),
            hidden_id: $("#hidden_id").val(),
            type: "password"
        };
        if ($("#hidden_id").val() != '') {
            $.ajax({
                url: '/create_consultant',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                data: invoice,
                dataType: 'JSON',
                success: function (data) {
                    var status=JSON.stringify(data['status']);
                    if(status=='false') {
                        $.each(data.errors,function(index,value){
                            $("#" + index + "_error").show();
                            $("#" + index + "_error").text(value[0]);
                        });
                    } else if(status==1) {
                        $("#old_password_error").show();
                        $("#old_password_error").text('Enter Correct Password');
                    } else {
                        $("#confirm_password_error").hide();
                        $("#old_password_error").hide();
                    }
                }
            });
        } else {
            alert("Please save page setting first");
        }
    });
</script>
@endsection