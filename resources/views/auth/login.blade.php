@extends('layout.public')
@section('title', $title)
@section('description', $description)
@section('content')

<div class="login-sec d-flex flex-column">
    <img src="{{ asset('images/logo-.png')}}" alt="logo"/>
    <h2>Login</h2>
    <span style="text-align: center!important;">with your GoToConsult account</span>
    <form id="login-form" method="POST" action="{{ route('login') }}">
        @csrf
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email"  name="email" value="{{ old('email') }}" required>
            @if ($alert = Session::get('alert-success'))
            <div class="alert alert-warning">{{ $alert }}</div>
            @endif
            @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert"><strong>{{ $errors->first('email') }}</strong></span>
            @endif
        </div>
        <div class="form-group password-sec">
            <input type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" id="pwd" name="password" placeholder="Password *" required>
            @if ($errors->has('password'))
            <span class="invalid-feedback" role="alert"><strong>{{ $errors->first('password') }}</strong></span>
            @endif
            <div class="forgot-pass d-flex">
                <a href="{{ url('password/reset') }}">Forgot password?</a>
            </div>
        </div>
        <div class="form-btn d-flex">
            <a class="nav-link" href="{{ route('register') }}">Create Account</a>
			<button type="submit" class="btn btn-primary login">{{ __('Login') }}</button>
        </div>
    </form>
</div>
		
<div class="bottom-link d-flex">
    <a href="{{ route('home') }}">Back to Home</a>
    <a href="{{ route('privacy') }}">Privacy</a>
    <img src="{{ asset('images/dot.png')}}" alt="dot"/>
    <a href="{{ route('terms') }}">Terms</a>
</div>
@endsection

@section('scripts')
<script>
    $(".login").click(function (event) {
        event.preventDefault();
        if ($("#login-form").valid()) {
            $("#login-form").submit();
        }
    });
</script>
@endsection