@extends('layout.public')
@section('title', $title)
@section('description', $description)
@section('content')

<div class="create-account d-flex">
    <div class="create-account-left">
        <div class="login-sec create-acc d-flex flex-column">
            <img src="{{asset('images/logo-.png')}}" alt="logo"/>
            <h2>Create an Account</h2>
            <span>Welcome to GoToConsult!</span>
            
            <form id="register-form" method="POST" action="{{ url('register') }}">
                @csrf
                <div class="form-group check-form top-check">
                    <input type="checkbox" id="html1" name="checkbox">
                    <label for="html1">I am a consultant</label> 
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="first_name" placeholder="First name *" name="first_name" required>
                    @if($errors->has('first_name'))
                    <div class="alert alert-danger">
                        <ul>{{$errors->first('first_name')}}</ul>
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="last_name" placeholder="Last name *" name="last_name" required>
                    @if($errors->has('last_name'))
                    <div class="alert alert-danger">
                        <ul>{{$errors->first('last_name')}}</ul>
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <select id="industry_expertise"  class="drop-box" name="industry_expertise" required>
                        <option selected disabled hidden>Industry Expertise *</option>
                        <option value="psychologist">Psychologist</option>
                        <option value="economy">Economy</option>
                        <option value="lawyer">Lawyer</option>
                        <option value="doctor&nurse">Doctor & Nurse</option>
                        <option value="animal doctor">Animal Doctor</option>
                        <option value="astrology">Astrology</option>
                        <option value="teach">Teacher</option>
                    </select>
                </div>
                @if($errors->has('industry_expertise'))
                <div class="alert alert-danger">
                    <ul>{{$errors->first('industry_expertise')}}</ul>
                </div>
                @endif
                <div class="form-group">
                    <input type="text" class="form-control" id="phone" placeholder="Phone *" name="phone" required>
                    @if($errors->has('phone'))
                    <div class="alert alert-danger">
                        <ul>{{$errors->first('phone')}}</ul>
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" id="email" placeholder="E-mail *" name="email" required>
                    @if($errors->has('email'))
                    <div class="alert alert-danger">
                        <ul>{{$errors->first('email')}}</ul>
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" id="password" placeholder="Create password *" name="password" required>
                    @if($errors->has('password'))
                    <div class="alert alert-danger">
                        <ul>{{$errors->first('password')}}</ul>
                    </div>
                    @endif
                </div>
                <div class="form-group check-form top-check">			  
                    <div class="l-ch">
                        <input type="checkbox" id="html2">
                        <label for="html2"></label>
                        <small>I have read and I accept GoToConsult’s <b>Terms</b> and <b>Privacy.</b></small>
                    </div>
                </div>
                <div class="form-btn d-flex">
                    <a href="{{ route('login') }}">Login</a>
                    <button type="submit" class="btn btn-primary register">Create Account</button>
                </div>
            </form>
        </div>
        <div class="bottom-link c-a-bottom d-flex">
            <a href="{{ route('home') }}">Back to Home</a> 
            <a href="{{ route('privacy') }}">Privacy</a>
            <img src="{{asset('images/dot.png')}}" alt="dot"/>
            <a href="{{ route('terms') }}">Terms</a>
        </div>
    </div>        
    <div class="create-account-right">
        <h2>Benefits with GoToConsult</h2>
        <p><img src="{{ asset('images/logo-bg.png')}}" alt="logo"/>Lorem ipsum dolor sit amet consectetur</p>
        <p><img src="{{ asset('images/logo-bg.png')}}" alt="logo"/>Lorem ipsum dolor sit amet consectetur</p>
        <p><img src="{{ asset('images/logo-bg.png')}}" alt="logo"/>Lorem ipsum dolor sit amet consectetur</p>
        <p><img src="{{ asset('images/logo-bg.png')}}" alt="logo"/>Lorem ipsum dolor sit amet consectetur</p>
        <p><img src="{{ asset('images/logo-bg.png')}}" alt="logo"/>Lorem ipsum dolor sit amet consectetur</p>
        <p><img src="{{ asset('images/logo-bg.png')}}" alt="logo"/>Lorem ipsum dolor sit amet consectetur</p>
        <p><img src="{{ asset('images/logo-bg.png')}}" alt="logo"/>Lorem ipsum dolor sit amet consectetur</p>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $(".register").click(function (event) {
        event.preventDefault();
        if ($("#register-form").valid()) {
            $("#register-form").submit();
        }
    });
</script>
@endsection
