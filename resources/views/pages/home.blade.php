@extends('layout.public')
@section('title', $title)
@section('description', $description)
@section('content')

<div class="banner">
    <div class="col-12 no-padding">
        <img src="{{asset('images/home/banner-img.jpg')}}" alt="no-img" style="max-width:100%">
        <div class="banner-head">
            <h3>Get the  help you need, <br>for a little less money</h3>
            <a href="{{ route('find_consultant') }}"> <button class="btn">Find a Consult</button></a>
        </div>
    </div>
</div>

<div class="ec-full full-explore">
    <div class="wrap">
        <div class="explore-cate-cart d-flex flex-wrap">
            @foreach($categories as $category)
            <?php $route = $category->category_url ?>
                <div class="explore-carts d-flex flex-column">
                    <img src="{{asset($category->category_icon)}}" alt="no-img"/>
                    <h3>{{$category->category_name}}</h3>
                    <small></small>
                    <span>{{$category->category_description}}</span>
                    <small></small>
                    <a class="btn" href="{{url('/category/').'/'.$route}}">Finn din rådgiver</a>
                </div>
            @endforeach
        </div>
    </div>
    <div class="ec-full consult-steps">
        <div class="wrap">
            <div class="row">
                <div class="col-md-4 step-blk">
                    <div>
                        <img src="{{asset('images/home/count-1.png')}}" alt="no-img" />
                    </div>
                    <div class="content">
                        <h3>Create an account</h3>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Hic est quia neque, id quo ex quidem minima ipsum dolores eligendi commodi ea quibusdam, distinctio beatae eaque. Hic a debitis impedit?</p>
                    </div>
                </div>
                <div class="col-md-4 step-blk">
                    <div>
                        <img src="{{asset('images/home/count-2.png')}}" alt="no-img" />
                    </div>
                    <div class="content">
                        <h3>Connect with Consultant</h3>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Hic est quia neque, id quo ex quidem minima ipsum dolores eligendi commodi ea quibusdam, distinctio beatae eaque. Hic a debitis impedit?</p>
                    </div>
                </div>
                <div class="col-md-4 step-blk">
                    <div>
                        <img src="{{asset('images/home/count-3.png')}}" alt="no-img" />
                    </div>
                    <div class="content">
                        <h3>Get Started!</h3>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Hic est quia neque, id quo ex quidem minima ipsum dolores eligendi commodi ea quibusdam, distinctio beatae eaque. Hic a debitis impedit?</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="full-cart avail-consultant">
    <div class="wrap">
        <h3 class="title">Available consultants right now:</h3>
        <div class="cart-full d-flex flex-wrap">
            <div class="cart-section d-flex flex-column">
                <img src="{{asset('images/home/person.png')}}" alt="no-img" />
                <h3>Lawyer</h3>
                <h5>Ola Normann</h5>
                <small></small>
                <h3>Call:<span>918 99 918</span></h3>
                <h3>Code:<span>369</span></h3>
                <small></small>
                <div class="star-images d-flex">
                    <ul class="d-flex">
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img" /></li>
                    </ul>
                </div>
                <small></small>
                <div class="rm d-flex">
                    <a href="#">Read more</a>
                    <p>39,90 kr p/m</p>
                </div>
                <div class="end-button d-flex">
                    <button class="btn"><img src="{{asset('images/home/ph.png')}}" alt="no-img" /></button>
                    <button class="btn btn-mid"><img src="{{asset('images/home/video.png')}}" alt="no-img" /></button>
                    <button class="btn"><img src="{{asset('images/home/msg.png')}}" alt="no-img" /></button>
                </div>
            </div>
            <div class="cart-section d-flex flex-column">
                <img src="{{asset('images/home/person.png')}}" alt="no-img" />
                <h3>Lawyer</h3>
                <h5>Ola Normann</h5>
                <small></small>
                <h3>Call:<span>918 99 918</span></h3>
                <h3>Code:<span>369</span></h3>
                <small></small>
                <div class="d-flex star-images">
                    <ul class="d-flex">
                        <li><img src="{{asset('images/home/star-r.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img" /></li>
                    </ul>
                </div>
                <small></small>
                <div class="rm d-flex">
                    <a href="#">Read more</a>
                    <p>39,90 kr p/m</p>
                </div>
                <div class="end-button d-flex">
                    <button class="btn"><img src="{{asset('images/home/ph.png')}}" alt="no-img" /></button>
                    <button class="btn btn-mid"><img src="{{asset('images/home/video.png')}}" alt="no-img" /></button>
                    <button class="btn"><img src="{{asset('images/home/msg.png')}}" alt="no-img" /></button>
                </div>
            </div>
            <div class="cart-section d-flex flex-column">
                <img src="{{asset('images/home/person.png')}}" alt="no-img" />
                <h3>Lawyer</h3>
                <h5>Ola Normann</h5>
                <small></small>
                <h3>Call:<span>918 99 918</span></h3>
                <h3>Code:<span>369</span></h3>
                <small></small>
                <div class="d-flex star-images">
                    <ul class="d-flex">
                        <li><img src="{{asset('images/home/star-y.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-y.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-y.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img" /></li>
                    </ul>
                </div>
                <small></small>
                <div class="rm d-flex">
                    <a href="#">Read more</a>
                    <p>39,90 kr p/m</p>
                </div>
                <div class="end-button d-flex">
                    <button class="btn"><img src="{{asset('images/home/ph.png')}}" alt="no-img" /></button>
                    <button class="btn btn-mid"><img src="{{asset('images/home/video.png')}}" alt="no-img" /></button>
                    <button class="btn"><img src="{{asset('images/home/msg.png')}}" alt="no-img" /></button>
                </div>
            </div>
            <div class="cart-section d-flex flex-column">
                <img src="{{asset('images/home/person.png')}}" alt="no-img" />
                <h3>Lawyer</h3>
                <h5>Ola Normann</h5>
                <small></small>
                <h3>Call:<span>918 99 918</span></h3>
                <h3>Code:<span>369</span></h3>
                <small></small>
                <div class="d-flex star-images">
                    <ul class="d-flex">
                        <li><img src="{{asset('images/home/star-o.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-o.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img" /></li>
                    </ul>
                </div>
                <small></small>
                <div class="rm d-flex">
                    <a href="#">Read more</a>
                    <p>39,90 kr p/m</p>
                </div>
                <div class="end-button d-flex">
                    <button class="btn"><img src="{{asset('images/home/ph.png')}}" alt="no-img" /></button>
                    <button class="btn btn-mid"><img src="{{asset('images/home/video.png')}}" alt="no-img" /></button>
                    <button class="btn"><img src="{{asset('images/home/msg.png')}}" alt="no-img" /></button>
                </div>
            </div>
        </div>
        <h3  class="title"> Consultants with best reviews:</h3>
        <div class="cart-full d-flex flex-wrap">
            <div class="cart-section d-flex flex-column">
                <img src="{{asset('images/home/person.png')}}" alt="no-img" />
                <h3>Lawyer</h3>
                <h5>Ola Normann</h5>
                <small></small>
                <h3>Call:<span>918 99 918</span></h3>
                <h3>Code:<span>369</span></h3>
                <small></small>
                <div class="d-flex star-images">
                    <ul class="d-flex">
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img" /></li>
                    </ul>
                </div>
                <small></small>
                <div class="rm d-flex">
                    <a href="#">Read more</a>
                    <p>39,90 kr p/m</p>
                </div>
                <div class="end-button d-flex">
                    <button class="btn"><img src="{{asset('images/home/ph.png')}}" alt="no-img" /></button>
                    <button class="btn btn-mid"><img src="{{asset('images/home/video.png')}}" alt="no-img" /></button>
                    <button class="btn"><img src="{{asset('images/home/msg.png')}}" alt="no-img" /></button>
                </div>
            </div>
            <div class="cart-section d-flex flex-column">
                <img src="{{asset('images/home/person.png')}}" alt="no-img" />
                <h3>Lawyer</h3>
                <h5>Ola Normann</h5>
                <small></small>
                <h3>Call:<span>918 99 918</span></h3>
                <h3>Code:<span>369</span></h3>
                <small></small>
                <div class="star-images d-flex">
                    <ul class="d-flex">
                        <li><img src="{{asset('images/home/star-dg.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-dg.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-dg.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-dg.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-dg.png')}}" alt="no-img" /></li>
                    </ul>
                </div>
                <small></small>
                <div class="rm d-flex">
                    <a href="#">Read more</a>
                    <p>39,90 kr p/m</p>
                </div>
                <div class="end-button d-flex">
                    <button class="btn"><img src="{{asset('images/home/ph-y.png')}}" alt="no-img" /></button>
                    <button class="btn btn-mid"><img src="{{asset('images/home/video-y.png')}}" alt="no-img" /></button>
                    <button class="btn"><img src="{{asset('images/home/msg-y.png')}}" alt="no-img" /></button>
                </div>
            </div>
            <div class="cart-section d-flex flex-column">
                <img src="{{asset('images/home/person.png')}}" alt="no-img" />
                <h3>Lawyer</h3>
                <h5>Ola Normann</h5>
                <small></small>
                <h3>Call:<span>918 99 918</span>
                </h3>
                <h3>Code:<span>369</span></h3>
                <small></small>
                <div class="star-images d-flex">
                    <ul class="d-flex">
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img" /></li>
                    </ul>
                </div>
                <small></small>
                <div class="rm d-flex">
                    <a href="#">Read more</a>
                    <p>39,90 kr p/m</p>
                </div>
                <div class="end-button d-flex">
                    <button class="btn"><img src="{{asset('images/home/ph-g.png')}}" alt="no-img" /></button>
                    <button class="btn btn-mid"><img src="{{asset('images/home/video-g.png')}}" alt="no-img" /></button>
                    <button class="btn"><img src="{{asset('images/home/msg-g.png')}}" alt="no-img" /></button>
                </div>
            </div>
            <div class="cart-section d-flex flex-column">
                <img src="{{asset('images/home/person.png')}}" alt="no-img" />
                <h3>Lawyer</h3>
                <h5>Ola Normann</h5>
                <small></small>
                <h3>Call:<span>918 99 918</span></h3>
                <h3>Code:<span>369</span></h3>
                <small></small>
                <div class="star-images d-flex">
                    <ul class="d-flex">
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img" /></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img" /></li>
                    </ul>
                </div>
                <small></small>
                <div class="rm d-flex">
                    <a href="#">Read more</a>
                    <p>39,90 kr p/m</p>
                </div>
                <div class="end-button d-flex">
                    <button class="btn"><img src="{{asset('images/home/ph.png')}}" alt="no-img" /></button>
                    <button class="btn btn-mid"><img src="{{asset('images/home/video.png')}}" alt="no-img" /></button>
                    <button class="btn"><img src="{{asset('images/home/msg.png')}}" alt="no-img" /></button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="full-cart search-counsult">
    <div class="wrap">
        <div class="row">
            <div class="col-md-6 no-padding">
                <div class="content">
                    <h3>Get in Touch<br> with a Consultant.</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Error illo inventore porro odio sit voluptate
                        doloremque totam sapiente laboriosam unde explicabo ipsum, id numquam earum iusto! Doloremque
                        repudiandae soluta eligendi.</p>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Error illo inventore porro odio sit voluptate
                        doloremque totam sapiente laboriosam unde explicabo ipsum, id numquam earum iusto! Doloremque
                        repudiandae soluta eligendi.</p>
                    <a href="{{ route('find_consultant') }}"> <button class="btn">Find a Consult</button></a>
                </div>
            </div>
            <div class="col-md-6 no-padding">
                <img src="{{asset('images/home/consult-img1.png')}}" alt="no-img" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 no-padding">
                <img src="{{asset('images/home/consult-img2.png')}}" alt="no-img" />
            </div>
            <div class="col-md-6 no-padding">
                <div class="content">
                    <h3>Get the help you
                        <br> need instatntly</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Error illo inventore porro odio sit voluptate
                        doloremque totam sapiente laboriosam unde explicabo ipsum, id numquam earum iusto! Doloremque
                        repudiandae soluta eligendi.</p>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Error illo inventore porro odio sit voluptate
                        doloremque totam sapiente laboriosam unde explicabo ipsum, id numquam earum iusto! Doloremque
                        repudiandae soluta eligendi.</p>
                    <a href="{{ route('find_consultant') }}"> <button class="btn">Find a Consult</button></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 no-padding">
                <div class="content">
                    <h3>Become a Consultant
                        <br>Help Others.</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Error illo inventore porro odio sit voluptate
                        doloremque totam sapiente laboriosam unde explicabo ipsum, id numquam earum iusto! Doloremque
                        repudiandae soluta eligendi.</p>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Error illo inventore porro odio sit voluptate
                        doloremque totam sapiente laboriosam unde explicabo ipsum, id numquam earum iusto! Doloremque
                        repudiandae soluta eligendi.</p>
                    <button class="btn">Become a Consult</button>
                </div>
            </div>
            <div class="col-md-6  no-padding">
                <img src="{{asset('images/home/consult-img3.png')}}" alt="no-img" />
            </div>
        </div>
    </div>
</div>

<div class="full-cart benifits">
    <div class="wrap">
        <div class="cart-full">
            <h3>Benefits of using GoToConsult</h3>
            <div class="row">
                <div class="col-lg-3 col-md-5 cart-section">
                    <img src="{{asset('images/logo-bg.png')}}" alt="logo">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum voluptatibus, distinctio nisi similique
                        saepe architecto modi labore sequi accusamus debitis suscipit dicta non, deserunt dolorum aspernatur,
                        odio dignissimos earum animi.</p>
                </div>
                <div class="col-lg-3 col-md-5 cart-section">
                    <img src="{{asset('images/logo-bg.png')}}" alt="logo">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum voluptatibus, distinctio nisi similique
                        saepe architecto modi labore sequi accusamus debitis suscipit dicta non, deserunt dolorum aspernatur,
                        odio dignissimos earum animi.</p>
                </div>
                <div class="col-lg-3 col-md-5 cart-section">
                    <img src="{{asset('images/logo-bg.png')}}" alt="logo">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum voluptatibus, distinctio nisi similique
                        saepe architecto modi labore sequi accusamus debitis suscipit dicta non, deserunt dolorum aspernatur,
                        odio dignissimos earum animi.</p>
                </div>
                <div class="col-lg-3 col-md-5 cart-section">
                    <img src="{{asset('images/logo-bg.png')}}" alt="logo">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum voluptatibus, distinctio nisi similique
                        saepe architecto modi labore sequi accusamus debitis suscipit dicta non, deserunt dolorum aspernatur,
                        odio dignissimos earum animi.</p>
                </div>
            </div>
            <div class="consult-btn">
                <button class="btn">Find a Consult</button>
                <button class="btn">Become a Consult</button>
            </div>
        </div>
    </div>
</div>

<div class="full-cart gd-ppl-words">
    <div class="wrap">
        <div class="cart-full">
            <h3>Good people. Good words</h3>
            <div class="row">
                <div class="col-lg-3 col-md-5 cart-section">
                    <div class="content">
                        <img src="{{asset('images/home/christine.png')}}" alt="logo">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum voluptatibus, distinctio nisi similique
                            saepe architecto modi labore sequi accusamus debitis suscipit dicta non, deserunt dolorum aspernatur,
                            odio dignissimos earum animi.</p>
                        <p>Sara C.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-5 cart-section">
                    <div class="content">
                        <img src="{{asset('images/home/christine.png')}}" alt="logo">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum voluptatibus, distinctio nisi similique
                            saepe architecto modi labore sequi accusamus debitis suscipit dicta non, deserunt dolorum aspernatur,
                            odio dignissimos earum animi.</p>
                        <p>Sara C.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-5 cart-section">
                    <div class="content">
                        <img src="{{asset('images/home/christine.png')}}" alt="logo">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum voluptatibus, distinctio nisi similique
                            saepe architecto modi labore sequi accusamus debitis suscipit dicta non, deserunt dolorum aspernatur,
                            odio dignissimos earum animi.</p>
                        <p>Sara C.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-5 cart-section">
                    <div class="content">
                        <img src="{{asset('images/home/christine.png')}}" alt="logo">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum voluptatibus, distinctio nisi similique
                            saepe architecto modi labore sequi accusamus debitis suscipit dicta non, deserunt dolorum aspernatur,
                            odio dignissimos earum animi.</p>
                        <p>Sara C.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
