@extends('layout.public')
@section('title', $title)
@section('description', $description)
@section('content')
<div class="banner">
    <div class="col-12 no-padding">
        <img src="{{asset('images/about-banner.png')}}" alt="no-img" style="max-width:100%">
        <div class="banner-head">
            <h3>We are <br> GoToConsult</h3>
            <a href=""><button class="btn">What do we solve</button></a>
        </div>
    </div>
</div>
    
<div class="full-cart benifits solve">
    <div class="wrap">
        <div class="cart-full">
            <h3>What do we solve?</h3>
            <div class="row">
                <div class="col-lg-3 col-md-5 cart-section">
                    <img src="{{asset('images/logo-bg.png')}}" alt="logo">
                    <h5>Lorem ipsum dolor</h5>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                </div>
                <div class="col-lg-3 col-md-5 cart-section">
                    <img src="{{asset('images/logo-bg.png')}}" alt="logo">
                    <h5>Lorem ipsum dolor</h5>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                </div>
                <div class="col-lg-3 col-md-5 cart-section">
                    <img src="{{asset('images/logo-bg.png')}}" alt="logo">
                    <h5>Lorem ipsum dolor</h5>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                </div>
                <div class="col-lg-3 col-md-5 cart-section">
                    <img src="{{asset('images/logo-bg.png')}}" alt="logo">
                    <h5>Lorem ipsum dolor</h5>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                </div>
            </div>
        </div>
    </div>
</div>
   
<div class="full-cart our_story">
    <div class="wrap">
        <div class="cart-full">
            <div class="row">
                <div class="col-lg-6 col-md-6 cart-section">
                    <h6>Our Story</h6>
                    <h3>How it all, <br>started in 2018.</h3>
                    <img src="{{asset('images/mobile.png')}}" alt="logo">
                </div>
                <div class="col-lg-6 col-md-6 cart-section">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="full-cart our_team">
    <div class="wrap">
        <div class="cart-full">
            <h6>Our team</h6>
            <h3>We value Paasion,
                <br> Innovation and technology. </h3>
            <div class="row">
                <div class="col-lg-4 col-md-4 cart-section">
                    <img src="{{asset('images/team-member.png')}}" alt="logo">
                    <h6>Mari Ettrnavn</h6>
                    <p>Cheif Executive Officer</p>
                    <span class="show_hide_bio1" onclick="itemshow(1)">Hide Bio</span>
                    <p class="bio_content1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum voluptatibus, distinctio nisi similique
                        saepe architecto modi labore sequi accusamus debitis suscipit dicta non, deserunt dolorum aspernatur,
                        odio dignissimos earum animi.</p>
                </div>
                <div class="col-lg-4 col-md-4 cart-section">
                    <img src="{{asset('images/team-member.png')}}" alt="logo">
                    <h6>Mari Ettrnavn</h6>
                    <p>Cheif Executive Officer</p>
                    <span class="show_hide_bio2" onclick="itemshow(2)">Hide Bio</span>
                    <p class="bio_content2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum voluptatibus, distinctio nisi similique
                        saepe architecto modi labore sequi accusamus debitis suscipit dicta non, deserunt dolorum aspernatur,
                        odio dignissimos earum animi.</p>
                </div>
                <div class="col-lg-4 col-md-4 cart-section">
                    <img src="{{asset('images/team-member.png')}}" alt="logo">
                    <h6>Mari Ettrnavn</h6>
                    <p>Cheif Executive Officer</p>
                    <span  class="show_hide_bio3" onclick="itemshow(3)">Hide Bio</span>
                    <p class="bio_content3"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum voluptatibus, distinctio nisi similique
                        saepe architecto modi labore sequi accusamus debitis suscipit dicta non, deserunt dolorum aspernatur,
                        odio dignissimos earum animi.</p>
                </div>
            </div>
        </div>
    </div>
</div>
  
<div class="ec-full consult-steps">
    <div class="wrap">
        <h5>It’s easy to get started</h5>
        <div class="row">
            <div class="col-md-4 step-blk">
                <div>
                    <img src="{{asset('images/home/count-1.png')}}" alt="no-img" />
                </div>
                <div class="content">
                    <h3>Create an account</h3>
                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Hic est quia neque, id quo ex quidem minima
                        ipsum dolores eligendi commodi ea quibusdam, distinctio beatae eaque. Hic a debitis impedit?</p>
                </div>
            </div>
            <div class="col-md-4 step-blk">
                <div>
                    <img src="{{asset('images/home/count-2.png')}}" alt="no-img" />
                </div>
                <div class="content">
                    <h3>Connect with Consultant</h3>
                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Hic est quia neque, id quo ex quidem minima
                        ipsum dolores eligendi commodi ea quibusdam, distinctio beatae eaque. Hic a debitis impedit?</p>
                </div>
            </div>
            <div class="col-md-4 step-blk">
                <div>
                    <img src="{{asset('images/home/count-3.png')}}" alt="no-img" />
                </div>
                <div class="content">
                    <h3>Get Started!</h3>
                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Hic est quia neque, id quo ex quidem minima
                        ipsum dolores eligendi commodi ea quibusdam, distinctio beatae eaque. Hic a debitis impedit?</p>
                </div>
            </div>
        </div>
        <div class="consult-btn">
            <button class="btn">Find a Consult</button>
            <button class="btn">Become a Consult</button>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    function itemshow(x) {
        if ($(".show_hide_bio"+x).text() == "Hide Bio") {
            $(".show_hide_bio"+x).html("Show Bio");
        } else {
            $(".show_hide_bio"+x).html("Hide Bio");
        }
        $(".bio_content"+x).slideToggle(); 
    }
</script>
@endsection