@extends('layout.public')
@section('title', $title)
@section('description', $description)
@section('content')
<div class="wrap">
    <div class="cate-info-page d-flex">
        <div class="left-ham d-flex">
            <img src="{{asset($category->category_icon)}}" alt="no_img"/>
        </div>
        <div class="ham-content d-flex flex-column">
            <h3>{{$category->category_name}}</h3>
            <span>Do you have questions about business law, housing,
            cabin, working life, heritage or other things? Our 
            lawyers help you with the questions you are wondering 
            about. Get in touch with a lawyer today. Call and get 
            started!</span>
        </div>
    </div>
</div>
    
<div class="pages-list d-flex">
    <div class="pages-list-l d-flex">
        <div class="pages-left d-flex">
            <h2>Showing 12 of 12 <span>{{$category->category_name}}</span></h2>
        </div>
        <div class="pages-right d-flex flex-column wrap-d">
            <span>Sort by</span>
            <div class="dropdown">
                <button type="button" class="btn p-l dropdown-toggle" data-toggle="dropdown">
                    Dropdown button
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">Link 1</a>
                    <a class="dropdown-item" href="#">Link 2</a>
                    <a class="dropdown-item" href="#">Link 3</a>
                </div>
            </div>
        </div>
    </div>
</div>
		
<div class="full-cart">
    <div class="wrap">
        <div class="cart-full d-flex flex-wrap">
            <div class="cart-section d-flex flex-column">
                <img src="{{asset('images/home/person.png')}}" alt="no-img"/>
                <h3>{{$category->category_name}}</h3>
                <h5>Ola Normann</h5>
                <small></small>
                <h3>Call: <span>918 99 918</span></h3>
                <h3>Code: <span>369</span></h3>
                <small></small>
                <div class="star-images d-flex">
                    <ul class="d-flex">
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img"/></li>
                    </ul>
                </div>
                    <small></small>
                <div class="rm d-flex">
                    <a href="#">Read more</a>
                    <p>39,90 kr p/m</p>
                </div>
                <div class="end-button d-flex">
                    <button class="btn"><img src="{{asset('images/home/ph.png')}}" alt="no-img"/></button>
                    <button class="btn btn-mid"><img src="{{asset('images/home/video.png')}}" alt="no-img"/></button>
                    <button class="btn"><img src="{{asset('images/home/msg.png')}}" alt="no-img"/></button>
                </div>
            </div>
            <div class="cart-section d-flex flex-column">
                <img src="{{asset('images/home/person.png')}}" alt="no-img"/>
                <h3>{{$category->category_name}}</h3>
                <h5>Ola Normann</h5>
                <small></small>
                <h3>Call: <span>918 99 918</span></h3>
                <h3>Code: <span>369</span></h3>
                <small></small>
                <div class="star-images d-flex">
                    <ul class="d-flex">
                        <li><img src="{{asset('images/home/star-r.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img"/></li>
                    </ul>
                </div>
                    <small></small>
                <div class="rm d-flex">
                    <a href="#">Read more</a>
                    <p>39,90 kr p/m</p>
                </div>
                <div class="end-button d-flex">
                    <button class="btn"><img src="{{asset('images/home/ph.png')}}" alt="no-img"/></button>
                    <button class="btn btn-mid"><img src="{{asset('images/home/video.png')}}" alt="no-img"/></button>
                    <button class="btn"><img src="{{asset('images/home/msg.png')}}" alt="no-img"/></button>
                </div>
            </div>
            <div class="cart-section d-flex flex-column">
                <img src="{{asset('images/home/person.png')}}" alt="no-img"/>
                <h3>{{$category->category_name}}</h3>
                <h5>Ola Normann</h5>
                <small></small>
                <h3>Call: <span>918 99 918</span></h3>
                <h3>Code: <span>369</span></h3>
                <small></small>
                <div class="star-images d-flex">
                    <ul class="d-flex">
                        <li><img src="{{asset('images/home/star-y.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-y.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-y.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img"/></li>
                    </ul>
                </div>
                    <small></small>
                <div class="rm d-flex">
                    <a href="#">Read more</a>
                    <p>39,90 kr p/m</p>
                </div>
                <div class="end-button d-flex">
                    <button class="btn"><img src="{{asset('images/home/ph.png')}}" alt="no-img"/></button>
                    <button class="btn btn-mid"><img src="{{asset('images/home/video.png')}}" alt="no-img"/></button>
                    <button class="btn"><img src="{{asset('images/home/msg.png')}}" alt="no-img"/></button>
                </div>
            </div>
            <div class="cart-section d-flex flex-column">
                <img src="{{asset('images/home/person.png')}}" alt="no-img"/>
                <h3>{{$category->category_name}}</h3>
                <h5>Ola Normann</h5>
                <small></small>
                <h3>Call: <span>918 99 918</span></h3>
                <h3>Code: <span>369</span></h3>
                <small></small>
                <div class="star-images d-flex">
                    <ul class="d-flex">
                        <li><img src="{{asset('images/home/star-o.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-o.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img"/></li>
                    </ul>
                </div>
                    <small></small>
                <div class="rm d-flex">
                    <a href="#">Read more</a>
                    <p>39,90 kr p/m</p>
                </div>
                <div class="end-button d-flex">
                    <button class="btn"><img src="{{asset('images/home/ph.png')}}" alt="no-img"/></button>
                    <button class="btn btn-mid"><img src="{{asset('images/home/video.png')}}" alt="no-img"/></button>
                    <button class="btn"><img src="{{asset('images/home/msg.png')}}" alt="no-img"/></button>
                </div>
            </div>
        </div>
        <div class="cart-full d-flex flex-wrap">
            <div class="cart-section d-flex flex-column">
                <img src="{{asset('images/home/person.png')}}" alt="no-img"/>
                <h3>{{$category->category_name}}</h3>
                <h5>Ola Normann</h5>
                <small></small>
                <h3>Call: <span>918 99 918</span></h3>
                <h3>Code: <span>369</span></h3>
                <small></small>
                <div class="star-images d-flex">
                    <ul class="d-flex">
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img"/></li>
                    </ul>
                </div>
                <small></small>
                <div class="rm d-flex">
                    <a href="#">Read more</a>
                    <p>39,90 kr p/m</p>
                </div>
                <div class="end-button d-flex">
                    <button class="btn"><img src="{{asset('images/home/ph.png')}}" alt="no-img"/></button>
                    <button class="btn btn-mid"><img src="{{asset('images/home/video.png')}}" alt="no-img"/></button>
                    <button class="btn"><img src="{{asset('images/home/msg.png')}}" alt="no-img"/></button>
                </div>
            </div>
            <div class="cart-section d-flex flex-column">
                <img src="{{asset('images/home/person.png')}}" alt="no-img"/>
                <h3>{{$category->category_name}}</h3>
                <h5>Ola Normann</h5>
                <small></small>
                <h3>Call: <span>918 99 918</span></h3>
                <h3>Code: <span>369</span></h3>
                <small></small>
                <div class="star-images d-flex">
                    <ul class="d-flex">
                        <li><img src="{{asset('images/home/star-dg.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-dg.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-dg.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-dg.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-dg.png')}}" alt="no-img"/></li>
                    </ul>
                </div>
                <small></small>
                <div class="rm d-flex">
                    <a href="#">Read more</a>
                    <p>39,90 kr p/m</p>
                </div>
                <div class="end-button d-flex">
                    <button class="btn"><img src="{{asset('images/home/ph-y.png')}}" alt="no-img"/></button>
                    <button class="btn btn-mid"><img src="{{asset('images/home/video-y.png')}}" alt="no-img"/></button>
                    <button class="btn"><img src="{{asset('images/home/msg-y.png')}}" alt="no-img"/></button>
                </div>
            </div>
            <div class="cart-section d-flex flex-column">
                <img src="{{asset('images/home/person.png')}}" alt="no-img"/>
                <h3>{{$category->category_name}}</h3>
                <h5>Ola Normann</h5>
                <small></small>
                <h3>Call: <span>918 99 918</span></h3>
                <h3>Code: <span>369</span></h3>
                <small></small>
                <div class="star-images d-flex">
                    <ul class="d-flex">
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img"/></li>
                    </ul>
                </div>
                <small></small>
                <div class="rm d-flex">
                    <a href="#">Read more</a>
                    <p>39,90 kr p/m</p>
                </div>
                <div class="end-button d-flex">
                    <button class="btn"><img src="{{asset('images/home/ph-g.png')}}" alt="no-img"/></button>
                    <button class="btn btn-mid"><img src="{{asset('images/home/video-g.png')}}" alt="no-img"/></button>
                    <button class="btn"><img src="{{asset('images/home/msg-g.png')}}" alt="no-img"/></button>
                </div>
            </div>
            <div class="cart-section d-flex flex-column">
                <img src="{{asset('images/home/person.png')}}" alt="no-img"/>
                <h3>{{$category->category_name}}</h3>
                <h5>Ola Normann</h5>
                <small></small>
                <h3>Call: <span>918 99 918</span></h3>
                <h3>Code: <span>369</span></h3>
                <small></small>
                <div class="star-images d-flex">
                    <ul class="d-flex">
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img"/></li>
                    </ul>
                </div>
                <small></small>
                <div class="rm d-flex">
                    <a href="#">Read more</a>
                    <p>39,90 kr p/m</p>
                </div>
                <div class="end-button d-flex">
                    <button class="btn"><img src="{{asset('images/home/ph.png')}}" alt="no-img"/></button>
                    <button class="btn btn-mid"><img src="{{asset('images/home/video.png')}}" alt="no-img"/></button>
                    <button class="btn"><img src="{{asset('images/home/msg.png')}}" alt="no-img"/></button>
                </div>
            </div>
        </div>
         <div class="cart-full d-flex flex-wrap">
            <div class="cart-section d-flex flex-column">
                <img src="{{asset('images/home/person.png')}}" alt="no-img"/>
                <h3>{{$category->category_name}}</h3>
                <h5>Ola Normann</h5>
                <small></small>
                <h3>Call: <span>918 99 918</span></h3>
                <h3>Code: <span>369</span></h3>
                <small></small>
                <div class="star-images d-flex">
                    <ul class="d-flex">
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img"/></li>
                    </ul>
                </div>
                <small></small>
                <div class="rm d-flex">
                    <a href="#">Read more</a>
                    <p>39,90 kr p/m</p>
                </div>
                <div class="end-button d-flex">
                    <button class="btn"><img src="{{asset('images/home/ph.png')}}" alt="no-img"/></button>
                    <button class="btn btn-mid"><img src="{{asset('images/home/video.png')}}" alt="no-img"/></button>
                    <button class="btn"><img src="{{asset('images/home/msg.png')}}" alt="no-img"/></button>
                </div>
            </div>
            <div class="cart-section d-flex flex-column">
                <img src="{{asset('images/home/person.png')}}" alt="no-img"/>
                <h3>{{$category->category_name}}</h3>
                <h5>Ola Normann</h5>
                <small></small>
                <h3>Call: <span>918 99 918</span></h3>
                <h3>Code: <span>369</span></h3>
                <small></small>
                <div class="star-images d-flex">
                    <ul class="d-flex">
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img"/></li>
                    </ul>
                </div>
                    <small></small>
                <div class="rm d-flex">
                    <a href="#">Read more</a>
                    <p>39,90 kr p/m</p>
                </div>
                <div class="end-button d-flex">
                    <button class="btn"><img src="{{asset('images/home/ph-y.png')}}" alt="no-img"/></button>
                    <button class="btn btn-mid"><img src="{{asset('images/home/video-y.png')}}" alt="no-img"/></button>
                    <button class="btn"><img src="{{asset('images/home/msg-y.png')}}" alt="no-img"/></button>
                </div>
            </div>
            <div class="cart-section d-flex flex-column">
                <img src="{{asset('images/home/person.png')}}" alt="no-img"/>
                <h3>{{$category->category_name}}</h3>
                <h5>Ola Normann</h5>
                <small></small>
                <h3>Call: <span>918 99 918</span></h3>
                <h3>Code: <span>369</span></h3>
                <small></small>
                <div class="star-images d-flex">
                    <ul class="d-flex">
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img"/></li>
                    </ul>
                </div>
                <small></small>
                <div class="rm d-flex">
                    <a href="#">Read more</a>
                    <p>39,90 kr p/m</p>
                </div>
                <div class="end-button d-flex">
                    <button class="btn"><img src="{{asset('images/home/ph-g.png')}}" alt="no-img"/></button>
                    <button class="btn btn-mid"><img src="{{asset('images/home/video-g.png')}}" alt="no-img"/></button>
                    <button class="btn"><img src="{{asset('images/home/msg-g.png')}}" alt="no-img"/></button>
                </div>
            </div>        
            <div class="cart-section d-flex flex-column">
                <img src="{{asset('images/home/person.png')}}" alt="no-img"/>
                <h3>{{$category->category_name}}</h3>
                <h5>Ola Normann</h5>
                <small></small>
                <h3>Call: <span>918 99 918</span></h3>
                <h3>Code: <span>369</span></h3>
                <small></small>
                <div class="star-images d-flex">
                    <ul class="d-flex">
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-g.png')}}" alt="no-img"/></li>
                        <li><img src="{{asset('images/home/star-w.png')}}" alt="no-img"/></li>
                    </ul>
                </div>
                <small></small>
                <div class="rm d-flex">
                    <a href="#">Read more</a>
                    <p>39,90 kr p/m</p>
                </div>
                <div class="end-button d-flex">
                    <button class="btn"><img src="{{asset('images/home/ph.png')}}" alt="no-img"/></button>
                    <button class="btn btn-mid"><img src="{{asset('images/home/video.png')}}" alt="no-img"/></button>
                    <button class="btn"><img src="{{asset('images/home/msg.png')}}" alt="no-img"/></button>
                </div>
            </div>
        </div>
    </div>
</div>
   
<div class="wrap">
    <div class="people-words d-flex flex-column">
        <h3>Good people. Good words.</h3>
        <div class="four-sec d-flex">
            <div class="sec-words d-flex flex-column">
                <img src="{{asset('images/girl-pic.png')}}" alt="no-img"/>
                <p>"Lorem Ipsum is simply dummy text of the printing 
                and typesetting industry. Lorem Ipsum has been the 
                industry's standard dummy text ever since the 1500s."</p>
                <p>Sara C.</p>
            </div>
            <div class="sec-words d-flex flex-column">
                <img src="{{asset('images/girl-pic.png')}}" alt="no-img"/>
                <p>"Lorem Ipsum is simply dummy text of the printing 
                and typesetting industry. Lorem Ipsum has been the 
                industry's standard dummy text ever since the 1500s."</p>
                <p>Sara C.</p>
            </div>
            <div class="sec-words d-flex flex-column">
                <img src="{{asset('images/girl-pic.png')}}" alt="no-img"/>
                <p>"Lorem Ipsum is simply dummy text of the printing 
                and typesetting industry. Lorem Ipsum has been the 
                industry's standard dummy text ever since the 1500s."</p>
                <p>Sara C.</p>
            </div>
            <div class="sec-words d-flex flex-column">
                <img src="{{asset('images/girl-pic.png')}}" alt="no-img"/>
                <p>"Lorem Ipsum is simply dummy text of the printing 
                and typesetting industry. Lorem Ipsum has been the 
                industry's standard dummy text ever since the 1500s."</p>
                <p>Sara C.</p>
            </div>
        </div>
    </div>
</div>

<div class="ec-full category-info">
    <h3>Explore Categories</h3>
    <div class="wrap">
        @foreach($categories as $key => $category)
            @if($key%4 == 0)
            <div class="explore-cate-cart d-flex flex-wrap">
            @endif
            <div class="explore-carts d-flex flex-column">
                <img src="{{asset($category->category_icon)}}" alt="no-img"/>
                <h3>{{$category->category_name}}</h3>
                <small></small>
                <span>{{$category->category_description}}</span>
                <small></small>
                <button class="btn">Finn din rådgiver</button>
            </div>
            @if($key%4 == 3 || $key>4 && $key%4 == 2)
            </div>
            @endif
        @endforeach
    </div>
</div>
@endsection