@extends('layout.public')
@section('title', $title)
@section('description', $description)
@section('content')

<div class="banner">
    <div class="col-12 no-padding">
        <img src="{{asset('images/consultant/become_consultant.png')}}" alt="no-img" style="max-width:100%">
        <div class="banner-head">
            <h3>Become a Consultant</h3>
            <p>Lorem ipsum dolor sit amet consectetur ipsum dolor remi.</p>
            <a href="{{ route('find_consultant') }}"> <button class="btn">Our Consult Services</button></a>
        </div>
    </div>
</div>

<div class="full-consultant consultant-service">
    <div class="wrap">
        <div class="cart-full">
            <h3>Our Consultant Services</h3>
            <div class="row">
                <div class="col-lg-6 col-md-6 service-section">
                    <img src="{{asset('images/home/physio.png')}}" alt="no-img" />
                    <p><span>Psychologist</span> <br/> Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.</p>
                </div>
                <div class="col-lg-6 col-md-6 service-section">
                    <img src="{{asset('images/home/economy.png')}}" alt="no-img" />
                    <p><span>Economy</span> <br/> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 service-section">
                    <img src="{{asset('images/home/hammer.png')}}" alt="no-img" />
                    <p><span>Lawyer</span> <br/> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.</p>
                </div>
                <div class="col-lg-6 col-md-6 service-section">
                    <img src="{{asset('images/home/doctor.png')}}" alt="no-img" />
                    <p><span>Doctor & Nurse</span> <br/> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 service-section">
                    <img src="{{asset('images/home/animal.png')}}" alt="no-img" />
                    <p><span>Animal Doctor</span> <br/> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.</p>
                </div>
                <div class="col-lg-6 col-md-6 service-section">
                    <img src="{{asset('images/home/astrolog.png')}}" alt="no-img" />
                    <p><span>Astrolog</span> <br/> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 service-section">
                    <img src="{{asset('images/home/teacher.png')}}" alt="no-img" />
                    <p><span>Teacher</span> <br/> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.</p>
                </div>
                <div class="col-lg-6 col-md-6 service-section become">
                    <p>Become a Consultant</p>
                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-angle-down fa-w-10 fa-lg"><path fill="currentColor" d="M143 352.3L7 216.3c-9.4-9.4-9.4-24.6 0-33.9l22.6-22.6c9.4-9.4 24.6-9.4 33.9 0l96.4 96.4 96.4-96.4c9.4-9.4 24.6-9.4 33.9 0l22.6 22.6c9.4 9.4 9.4 24.6 0 33.9l-136 136c-9.2 9.4-24.4 9.4-33.8 0z" class=""></path></svg>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="full-consultant consultant-platform">
    <div class="wrap">
        <div class="cart-full">
            <div class="row">
                <div class="col-lg-5 col-md-6 platform-group">
                    <h3>A Consultantation Platform</h3>
                    <div class="platform">
                        <img src="{{asset('images/logo-bg.png')}}" alt="logo">
                        <p><span>Overview of your customers</span> <br/> Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                    <div class="platform">
                        <img src="{{asset('images/logo-bg.png')}}" alt="logo">
                        <p><span>Call, video or live chat</span> <br/> Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                    <div class="platform">
                        <img src="{{asset('images/logo-bg.png')}}" alt="logo">
                        <p><span>Consultant profile</span> <br/> Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                    <div class="platform">
                        <img src="{{asset('images/logo-bg.png')}}" alt="logo">
                        <p><span>Overview of invoices</span> <br/> Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                    <div class="platform">
                        <img src="{{asset('images/logo-bg.png')}}" alt="logo">
                        <p><span>Complete account control</span> <br/> Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                </div>
                <div class="col-lg-7 col-md-6">
                    <img src="{{asset('images/consultant/platform.png')}}" class="platform-img" alt="logo">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="full-consultant gd-ppl-words">
    <div class="wrap">
        <div class="cart-full">
            <h3>Happy Consultants using GoToConsult</h3>
            <div class="row">
                <div class="col-lg-3 col-md-5 cart-section">
                    <div class="content">
                        <img src="{{asset('images/home/christine.png')}}" alt="logo">
                        <p>"Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum voluptatibus, distinctio nisi similique
                            saepe architecto modi labore sequi accusamus debitis suscipit dicta non, deserunt dolorum aspernatur,
                            odio dignissimos earum animi."</p>
                        <p>Sara C.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-5 cart-section">
                    <div class="content">
                        <img src="{{asset('images/home/christine.png')}}" alt="logo">
                        <p>"Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum voluptatibus, distinctio nisi similique
                            saepe architecto modi labore sequi accusamus debitis suscipit dicta non, deserunt dolorum aspernatur,
                            odio dignissimos earum animi."</p>
                        <p>Sara C.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-5 cart-section">
                    <div class="content">
                        <img src="{{asset('images/home/christine.png')}}" alt="logo">
                        <p>"Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum voluptatibus, distinctio nisi similique
                            saepe architecto modi labore sequi accusamus debitis suscipit dicta non, deserunt dolorum aspernatur,
                            odio dignissimos earum animi."</p>
                        <p>Sara C.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-5 cart-section">
                    <div class="content">
                        <img src="{{asset('images/home/christine.png')}}" alt="logo">
                        <p>"Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum voluptatibus, distinctio nisi similique
                            saepe architecto modi labore sequi accusamus debitis suscipit dicta non, deserunt dolorum aspernatur,
                            odio dignissimos earum animi."</p>
                        <p>Sara C.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="full-consultant consultant-contact">
    <div class="wrap">
        <div class="cart-full">
            <div class="row">
                <div class="col-lg-6 col-md-6 contact-form">
                    <h3>Join as consultant and work from anywhere.</h3>
                    <form id="consultant-form" action="{{ url('join_consultant') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="text" class="form-control" name="first_name" placeholder="First name *" data-msg-required="First name required." required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="last_name" placeholder="Last name *" data-msg-required="Last name required." required>
                        </div>
                        <div class="form-group">
                            <!-- <input type="text" class="form-control" name="industry" placeholder="Industry Expertise *" data-msg-required="Industry selection required." required> -->
                            <select class="form-control drop-box" name="industry" data-msg-required="Industry selection required." required>
                                <option selected disabled hidden>Industry Expertise *</option>
                                <option value="psychologist">Psychologist</option>
                                <option value="economy">Economy</option>
                                <option value="lawyer">Lawyer</option>
                                <option value="doctor&nurse">Doctor & Nurse</option>
                                <option value="animal doctor">Animal Doctor</option>
                                <option value="astrology">Astrology</option>
                                <option value="teach">Teacher</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="number" class="form-control" name="phone" placeholder="Phone *" data-msg-required="Phone number is required." required>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" placeholder="E-mail *" data-msg-required="E-mail is required." required>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" placeholder="Create password *" data-msg-required="Password is required." required>
                        </div>
                        <div class="checkbox agree_check">
                            <label class="container">I have read and I accept<br/> GoToConsult's <a href="{{url('/terms')}}" target="_blank">Terms </a> and <a href="{{url('/privacy')}}" target="_blank">Privacy. </a><input type="checkbox"><span class="checkmark"></span></label>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn" value="Create a Consultant Account">
                        </div>
                    </form>
                </div>
                <div class="col-lg-6 col-md-6 requirements">
                    <label>Mandatory Rquirements:</label>
                    <div class="require-item">
                        <p>1.</p>
                        <p>Lorem ipsum dolor sit amet consectetur ipsum dolor remi order. Lorem ipsum is just a dummy text.</p>
                    </div>
                    <div class="require-item">
                        <p>2.</p>
                        <p>Lorem ipsum dolor sit amet consectetur ipsum dolor remi order. Lorem ipsum is just a dummy text.</p>
                    </div>
                    <div class="require-item">
                        <p>3.</p>
                        <p>Lorem ipsum dolor sit amet consectetur ipsum dolor remi order. Lorem ipsum is just a dummy text.</p>
                    </div>
                    <div class="require-item">
                        <p>4.</p>
                        <p>Lorem ipsum dolor sit amet consectetur ipsum dolor remi order. Lorem ipsum is just a dummy text.</p>
                    </div>
                    <div class="require-item">
                        <p>5.</p>
                        <p>Lorem ipsum dolor sit amet consectetur ipsum dolor remi order. Lorem ipsum is just a dummy text.</p>
                    </div>
                    <div class="require-item">
                        <p>6.</p>
                        <p>Lorem ipsum dolor sit amet consectetur ipsum dolor remi order. Lorem ipsum is just a dummy text.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $(".btn").click(function (event) {
        event.preventDefault();
        if ($("#consultant-form").valid()) {
            $("#consultant-form").submit();
        }
    });
</script>
@endsection