@extends('layout.member')
@section('title', $title)
@section('description', $description)
@section('content')

<div class="wrapper member-sidebar">
    @include('elements.member_sidebar')
    <div class="content-wrapper my-consultant-wrapper adminprof">
        <div class="content_holesecion">
            <div class="full-chat-section d-flex">
                <div id="chat-left-top1" class="chat-left-top">
                    <div class="chat-left d-flex flex-column">
                        <h2>My Customers</h2>
                        <div onclick="myFunction()" class="chat-model d-flex">
                            <div class="chat-icon">
                                <img src="{{asset('images/sara.png')}}" alt="no-image"/>
                            </div>
                            <div class="chat-details d-flex flex-column">
                                <b>Sara Louise <span>Available</span></b>
                                <legend>Lawyer</legend>
                                <p>Lorem Ipsum er rett og slett dummy</p>
                                <p>tekst fra og for trykkeindustrien..</p>
                            </div>
                        </div>
                        <div onclick="myFunction()" class="chat-model d-flex">
                            <div class="chat-icon">
                                <img src="{{asset('images/christine.png')}}" alt="no-image"/>
                            </div>
                            <div class="chat-details offline d-flex flex-column">
                                <b>Christine Sether<span>Offline</span></b>
                                <legend>Nurse</legend>
                                <p>Lorem Ipsum er rett og slett dummy</p>
                                <p>tekst fra og for trykkeindustrien..</p>
                            </div>
                        </div>
                        <div onclick="myFunction()" class="chat-model d-flex">
                            <div class="chat-icon">
                                <img src="{{asset('images/arman.png')}}" alt="no-image"/>
                            </div>
                            <div class="chat-details d-flex flex-column">
                                <b>Arman Elaoui<span>Available</span></b>
                                <legend>Lawyer</legend>
                                <p>Lorem Ipsum er rett og slett dummy</p>
                                <p>tekst fra og for trykkeindustrien..</p>
                            </div>
                        </div>
                        <div onclick="myFunction()" class="chat-model d-flex">
                            <div class="chat-icon">
                                <img src="{{asset('images/oldman.png')}}" alt="no-image"/>
                            </div>
                            <div class="chat-details in-call d-flex flex-column">
                                <b>Martin Espensen<span>In a call</span></b>
                                <legend>Psychologist</legend>
                                <p>Lorem Ipsum er rett og slett dummy</p>
                                <p>tekst fra og for trykkeindustrien..</p>
                            </div>
                        </div>
                        <div onclick="myFunction()" class="chat-model d-flex">
                            <div class="chat-icon">
                                <img src="{{asset('images/siri.png')}}" alt="no-image"/>
                            </div>
                            <div class="chat-details in-call d-flex flex-column">
                                <b>Siri Nilsen<span>In a call</span></b>
                                <legend>Animal Doctor</legend>
                                <p>Lorem Ipsum er rett og slett dummy</p>
                                <p>tekst fra og for trykkeindustrien..</p>
                            </div>
                        </div>
                        <div onclick="myFunction()" class="chat-model d-flex">
                            <div class="chat-icon">
                                <img src="{{asset('images/martin.png')}}" alt="no-image"/>
                            </div>
                            <div class="chat-details d-flex flex-column">
                                <b>Marte Pedersen<span>Available</span></b>
                                <legend>Teacher</legend>
                                <p>Lorem Ipsum er rett og slett dummy</p>
                                <p>tekst fra og for trykkeindustrien..</p>
                            </div>
                        </div>
                        <div onclick="myFunction()" class="chat-model d-flex">
                            <div class="chat-icon">
                                <img src="{{asset('images/abdel.png')}}" alt="no-image"/>
                            </div>
                            <div class="chat-details offline d-flex flex-column">
                                <b>Abdel Hadri<span>Offline</span></b>
                                <legend>Economist</legend>
                                <p>Lorem Ipsum er rett og slett dummy</p>
                                <p>tekst fra og for trykkeindustrien..</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="chat-right-top">
                    <div class="chat-right d-flex flex-column">
                        <div class="chat-profile d-flex flex-wrap">
                            <h2>Arman Elaoui</h2>
                            <span><img src="{{asset('images/time.png')}}" alt="no-image"/>03:42:34</span>
                            <span><img src="{{asset('images/numbers-icon.png')}}" alt="no-image"/>119,70 kr <small>/39,90 kr pr. min.</small></span>
                            <div class="end-chat-right d-flex">
                                <button class="btn">End session</button>
                                <img src="{{asset('images/call.png')}}" alt="no-image"/>
                                <img src="{{asset('images/video-call.png')}}" alt="no-image"/>
                            </div>
                        </div>
                        <div class="chat-histroy d-flex flex-column">
                            <div class="chat-heading d-flex">
                                <span>Your chat has started</span>
                            </div>
                            <div class="chat-list d-flex">
                                <p>Hi Arman! My name is Nohman. We want to get exclusive
                                rights to an idea we have. Where can we register this? Do you
                                know how long you can have exclusive rights to an idea? </p>
                                <img src="{{asset('images/chat-icon.png')}}"/>
                            </div>
                            <div class="chat-time d-flex">
                                <span>14:32, Tuesday</span>
                            </div>
                            <div class="chat-heading today d-flex">
                                <span>Today</span>
                            </div>
                            <div class="chat-list-left d-flex">
                                <img src="{{asset('images/arman.png')}}"/>
                                <p>Hi Nohman, Have you been in contact with the Brønnøysund?</p>
                            </div>
                            <div class="chat-time today d-flex">
                                <span>35 min ago</span>
                            </div>
                            <div class="chat-list d-flex">
                                <p>Hi Arman! No I have not. Should I Do It? Is it through
                                the Brønnøysund Register that I can register a trademark?</p>
                                <img src="{{asset('images/chat-icon.png')}}"/>
                            </div>
                            <div class="chat-time d-flex">
                                <span>31 min ago</span>
                            </div>
                            <div class="chat-heading ended d-flex">
                                <span>Your chat has ended</span>
                            </div>
                            <div class="rate-session d-flex flex-column">
                                <img src="{{asset('images/arman-2.png')}}" alt="no-image"/>
                                <h2>Rate this session</h2>
                                <div class="rate-stars d-flex">
                                    <img src="{{asset('images/home/star-g.png')}}" alt="no-image"/>
                                    <img src="{{asset('images/home/star-g.png')}}" alt="no-image"/>
                                    <img src="{{asset('images/home/star-g.png')}}" alt="no-image"/>
                                    <img src="{{asset('images/home/star-g.png')}}" alt="no-image"/>
                                    <img src="{{asset('images/home/star-w.png')}}" alt="no-image"/>
                                    <p>4.0</p>
                                </div>
                            </div>
                            <div class="write-text d-flex flex-column">
                                <span><input type="text" placeholder="Write message here...."/></span>
                                <div class="send-msg d-flex">
                                    <button class="btn">Send</button>
                                    <input type="checkbox" id="fruit1" name="fruit-1" value="Apple"/>
                                    <label id="sms" for="fruit1">SMS</label>
                                    <input type="checkbox" id="fruit4" name="fruit-4" value="Strawberry">
                                    <label id="inapp" for="fruit4">In-app</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="chatter-pro d-flex flex-column">
                    <div class="chatter-setting d-flex">
                        <img src="{{asset('images/setting-icon.png')}}"/>
                        <button type="button" class="close" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="rate-session d-flex flex-column">
                        <img class="arman-end" src="{{asset('images/arman-2.png')}}" alt="no-image"/>
                        <span class="absol-span">&#8226;</span>
                        <p>Lawyer</p>
                        <h2>Arman Elaoui</h2>
                        <small>39,90 kr p/m</small>
                        <div class="rate-stars d-flex">
                            <img src="{{asset('images/home/star-small-g.png')}}" alt="no-image"/>
                            <img src="{{asset('images/home/star-small-g.png')}}" alt="no-image"/>
                            <img src="{{asset('images/home/star-small-g.png')}}" alt="no-image"/>
                            <img src="{{asset('images/home/star-small-g.png')}}" alt="no-image"/>
                            <img src="{{asset('images/home/star-small-w.png')}}" alt="no-image"/>
                        </div>
                    </div>
                    <div class="chat-records d-flex">
                        <div class="records-left flex-column">
                            <h2>43</h2>
                            <span>completed chats</span>
                        </div>
                        <div class="records-right flex-column">
                            <h2>30 min</h2>
                            <span>last online</span>
                        </div>
                    </div>
                    <div class="chat-drop d-flex flex-column">
                        <div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle btn-user" data-toggle="dropdown">Details</button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#">Link 1</a>
                                <a class="dropdown-item" href="#">Link 2</a>
                                <a class="dropdown-item" href="#">Link 3</a>
                            </div>
                        </div>
                        <div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle btn-user" data-toggle="dropdown">Ratings</button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#">Link 1</a>
                                <a class="dropdown-item" href="#">Link 2</a>
                                <a class="dropdown-item" href="#">Link 3</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="js/adminlte.min.js"></script>
<script>
    function myFunction() {
        document.getElementById("chat-right-top").style.display = "block";
        document.getElementById("chat-left-top1").style.display = "none";
    }
</script>
@endsection