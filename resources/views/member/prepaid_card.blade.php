@extends('layout.member')
@section('title', $title)
@section('description', $description)
@section('content')

<div class="wrapper member-sidebar">
    @include('elements.member_sidebar')
    <div class="content-wrapper adminprof">
		<div class="content_holesecion">
		    <h2 class="pre-topic">Prepaid Card</h2>
			<div class="prepaid-card-full d-flex">
				<div class="prepaid-card-left">
                    <div class="pay-method d-flex flex-column">
                        <h3>Payment Method</h3>
                        <div class="method-image d-flex">
                            <img src="{{asset('images/visa.png')}}" alt="no-image"/>
                            <img src="{{asset('images/vpps.png')}}" alt="no-image"/>
                            <img src="{{asset('images/klarna.png')}}" alt="no-image"/>
                        </div>
                    </div>
				
                    <div class="pay-cust-info d-flex flex-column">
                        <h3>Contact information</h3>
                        <form name="prepaid-form" class="d-flex flex-wrap prepaid-form">
                            <div class="first m-t-pre d-flex flex-column">
                                <label>First name  *</label>
                                <input type="text" value=""/>
                            </div>
                            <div class="last d-flex flex-column">
                                <label>Last name  *</label>
                                <input type="text" value=""/>
                            </div>
                            <div class="email d-flex flex-column">
                                <label>E-mail *</label>
                                <input type="text" value=""/>
                            </div>
                        </form>
                    </div>
					<div class="pay-cust-info pre-pay-info d-flex flex-column">
                        <h3>Payment information</h3>
                        <form name="prepaid-form" class="d-flex flex-wrap prepaid-form">
                            <div class="email m-t-pre d-flex flex-column">
                                <label>Card number *</label>
                                <input type="text" value=""/>
                            </div>
                            <div class="expiry m-t-pre d-flex flex-column">
                                <label>Expiry Date *</label>
                                <input type="text" value=""/>
                            </div>
                            <div class="expiry expiry-month d-flex flex-column">
                                <label>Expiry Month *</label>
                                <input type="text" value=""/>
                            </div>
                            <div class="expiry d-flex flex-column">
                                <label>Expiry Year *</label>
                                <input type="text" value=""/>
                            </div>
                            <div class="email m-t-pre d-flex flex-column">
                                <label>Address *</label>
                                <input type="text" value=""/>
                            </div>
                            <div class="first d-flex flex-column">
                                <label>Zip code *</label>
                                <input type="text" value=""/>
                            </div>
                            <div class="last d-flex m-t-pre flex-column">
                                <label>Zip place *</label>
                                <input type="text" value=""/>
                            </div>
                            <div class="add-cart d-flex flex-column">
                                <button class="btn">Add Card</button>
                            </div>
                        </form>
				    </div>
			    </div>
                <div class="prepaid-card-right">
                    <div class="current-bal d-flex flex-column">
                        <h3>Current Balance</h3>
                        <span>3498,90 kr</span>
                        <button class="btn ac-btn">Add Card</button>
                    </div>
                </div>
			</div>
		</div>
    </div>
</div>

@endsection