@extends('layout.member')
@section('title', $title)
@section('description', $description)
@section('content')

<div class="wrapper member-sidebar">
    @include('elements.member_sidebar')
    <div class="content-wrapper adminprof">
		<div class="content_holesecion">
			<div class="single-page d-flex flex-column">
                <div class="single-page-heading log-settings-head d-flex flex-column">
                    <h2>Settings</h2>
                </div>
			
                <div class="profile-uploader d-flex flex-column">
                    <div class="profile-sec log-sec1 d-flex flex-column">
                        <h3>Profile Settings</h3>
                        <div class="imageupload log-setting-up">
                            <h3>Profile image</h3>
                            <label class="btn btn-file">
                                <button class="btn up-img">Upload an image</button>
                                <input type="file" name="image-file">
                            </label>
                        </div>
                        <div class="page-seting-content psc-log d-flex flex-column">
                            <label>First name</label>
                            <input type="text" name="faq" value="Nohman"/>
                            <label>Last name</label>
                            <input type="text" name="faq" value="Janjua"/>
                            <label>E-mail</label>
                            <input type="text" name="faq" value="nohman@fantasylab.io"/>
                            <label>Phone</label>
                            <input type="text" name="faq" value="454 94 649"/>
                            <label>Industry Expertise</label>
                            <input type="text" name="faq" value="Advokat"/>
                            <button class="sp-f cs save-btn btn">Save</button>
                        </div>
                    </div>
                </div>
			
                <div class="profile-uploader setting-uploader d-flex flex-column">
                    <div class="profile-sec contact-sec d-flex flex-column">
                        <h3>CONTACT SETTINGS</h3>
                        <div class="three-uploader d-flex">
                            <div class="up-one d-flex flex-column">
                            <label class="heading-t">Phone</label>
                                <label class="switch">
                                <input type="checkbox" checked>
                                <span class="slider"></span>
                                <span class="uncheck"></span>
                                </label>
                            </div>
                            <div class="up-two d-flex flex-column ">
                                <label class="heading-t">Chat</label>
                                <label class="switch">
                                <input type="checkbox">
                                <span class="slider"></span>
                                <span class="uncheck"></span>
                                </label>
                            </div>
                            <div class="up-three d-flex flex-column">
                                <label class="heading-t">Video</label>
                                <label class="switch">
                                <input type="checkbox" checked>
                                <span class="slider"></span>
                                <span class="uncheck"></span>
                                </label>
                            </div>
                        </div>
                        <button class="sp-f cs btn save-btn">Save</button>
                    </div>
                </div>
                <div class="page-setting d-flex flex-column">
                    <div class="page-seting-content login-invo-setting d-flex flex-column">
                        <h3>INVOICE SETTINGS</h3>
                        <label>Company name</label>
                        <input type="text" name="faq" value="FantasyLab AS"/>
                        <label>Invoice e-mail</label>
                        <input type="text" name="faq" value="invoice@fantasylab.io"/>
                        <label>First name</label>
                        <input type="text" name="faq" value="Nohman"/>
                        <label>Last name</label>
                        <input type="text" name="faq" value="Janjua"/>
                        <label>Address</label>
                        <input type="text" name="faq" value="Traverveien 11"/>
                        <label>Zip code</label>
                        <input type="text" name="faq" value="0588"/>
                        <label>Zip place</label>
                        <input type="text" name="faq" value="Oslo"/>
                        <button class="sp-f cs save-btn btn">Save</button>
                    </div>
                </div>
                <div class="page-setting meta-info d-flex flex-column">
                    <h2>CHANGE PASSWORD</h2>
                    <div class="page-seting-content d-flex flex-column">
                        <label>Old password</label>
                        <input type="password" name="faq" value=""/>
                        <label>New password</label>
                        <input type="password" name="faq" value=""/>
                        <button class="sp-f cs save-btn btn">Save</button>
                    </div>
                </div>
		    </div>	
		</div>
    </div>
</div>

@endsection