<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li id="customer_menu" class="{{ $active == '0' ? 'active' : '' }}">
                <a href="{{ route('customerlist')}}">
                    <i class="fa fa-users"></i>
                    <span>Customers</span>
                </a>
            </li>
            <li class="{{ $active == '1' ? 'active' : '' }}">
                <a href="{{ route('consultantlist')}}">
                    <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                    <span>Consultants</span>
                </a>
            </li>
            <li class="{{ $active == '2' ? 'active' : '' }}">
                <a href="{{ route('categorylist')}}">
                    <i class="fa fa-server" aria-hidden="true"></i>
                    <span>Categories</span>
                </a>
            </li>
            <li class="{{ $active == '3' ? 'active' : '' }}">
                <a href="{{ route('pagelist')}}">
                    <i class="fa fa-book" aria-hidden="true"></i>
                    <span>Pages</span>
                </a>
            </li>
            <li class="{{ $active == '4' ? 'active' : '' }}">
                <a href="{{ route('settings')}}">
                    <i class="fa fa-cogs" aria-hidden="true"></i>
                    <span>Settings</span>
                </a>
            </li>
        </ul>
    </section>
</aside>
