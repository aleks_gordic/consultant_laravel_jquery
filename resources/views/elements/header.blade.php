<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid wraper">
  <a class="navbar-brand" href="{{ route('home') }}"><img src="{{ asset('images/logo.png')}}" alt="logo"/></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo01">

      <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
        <li class="nav-item active">
          <div class="dropdown">
            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
              Categories
            </button>
            <div class="dropdown-menu">
              @foreach ($categories as $key => $category)
              <?php $route = $category->category_url ?>
              <a class="dropdown-item" href="{{url('/category/').'/'.$route}}">{{$category->category_name}}</a>
              @endforeach
            </div>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('become_consultant') }}">Become a Consultant </a>
        </li>
        <li class="nav-item">
          <a class="nav-link disabled" href="{{ route('about_us') }}">About us</a>
        </li>
        <li class="nav-item">
          <a class="nav-link disabled" href="{{ route('faq') }}">FAQ</a>
        </li>
      </ul>

      <div class="end-nav d-flex">
        @if(!auth()->user())
        <a class="nav-log" href="{{ route('login') }}">Login</a>
        <a href="{{ route('find_consultant') }}">	<button class="btn">Find a Consultant</button> </a>
        @endif
        {{--  <a class="notify-count" href=""> <img src="{{ asset('images/bell-icon.png')}}" alt="icon"/> <span>2</span></a>  --}}

        <div class="dropdown">
          <button type="button" class="btn btn-primary nav-right dropdown-toggle" data-toggle="dropdown">
            <img src="{{ asset('images/flag-icon.png')}}" alt="icon"/>Norsk
          </button>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="#">Link 1</a>
            <a class="dropdown-item" href="#">Link 2</a>
            <a class="dropdown-item" href="#">Link 3</a>
          </div>
        </div>
        @if(auth()->user())
        <div class="dropdown">
          <button type="button" class="btn btn-primary nav-right dropdown-toggle user-btn" data-toggle="dropdown">
            <img src="{{asset('images/user-profile.png')}}" alt="icon"><span>{{auth()->user()->first_name}} {{auth()->user()->last_name}}</span>
          </button>
          <div class="dropdown-menu profile-dropdown">
            @if(Auth::user()->role=='consultant')
            <a class="dropdown-item" href="{{url('/find_customer')}}">My Customers</a>
            @elseif(Auth::user()->role=='customer')
            <a class="dropdown-item" href="{{url('/find_consultant')}}">My Consultants</a>
            @endif
            <a class="dropdown-item" href="{{url('/prepaid_card')}}">Prepaid Card</a>
            <a class="dropdown-item" href="{{url('/invoice')}}">Invoices</a>
            <a class="dropdown-item" href="{{url('/prepaid_card_settings')}}">Settings</a>
            <a class="dropdown-item" href="{{url('/logout')}}">Logout</a>
          </div>
        </div>
        @endif
      </div>
    </div>
  </div>
</nav>
