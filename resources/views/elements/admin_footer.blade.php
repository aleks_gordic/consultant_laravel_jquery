<footer class="d-flex footer-section">
    <span>© 2019 Teletjenesten AS. All Rights Reserved.<b><img src="{{ asset('images/dot.png')}}"/>Terms</b> 
    <b><img src="{{ asset('images/dot.png')}}"/>Privacy</b></span>
    <h6>UI/UX Design & Full Stack Development: FantasyLab.</h6>
</footer>