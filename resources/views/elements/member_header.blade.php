<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid wraper">
  <a class="navbar-brand" href="{{ route('home') }}"><img src="{{ asset('images/logo.png')}}" alt="logo"/></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo01">

      <div class="end-nav d-flex ml-auto">
        <a href="{{ route('category_info',['id' => 8]) }}">	<button class="btn">Find a Consultant</button> </a>        
        {{--  <a class="notify-count" href=""> <img src="{{ asset('images/bell-icon.png')}}" alt="icon"/> <span>2</span></a>  --}}

        <div class="dropdown">
          <button type="button" class="btn btn-primary nav-right dropdown-toggle" data-toggle="dropdown">
            <img src="{{ asset('images/flag-icon.png')}}" alt="icon"/>Norsk
          </button>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="#">Link 1</a>
            <a class="dropdown-item" href="#">Link 2</a>
            <a class="dropdown-item" href="#">Link 3</a>
          </div>
        </div>
        @if(auth()->user())
        <div class="dropdown">
          <button type="button" class="btn btn-primary nav-right dropdown-toggle user-btn" data-toggle="dropdown">
            <img src="{{asset('images/user-profile.png')}}" alt="icon"><span>{{auth()->user()->first_name}} {{auth()->user()->last_name}}</span>
          </button>
          <div class="dropdown-menu profile-dropdown">
            @if(Auth::user()->role=='consultant')
            <a class="dropdown-item" href="{{url('/find_customer')}}">My Customers</a>
            @elseif(Auth::user()->role=='customer')
            <a class="dropdown-item" href="{{url('/find_consultant')}}">My Consultants</a>
            @endif
            <a class="dropdown-item" href="{{url('/prepaid_card')}}">Prepaid Card</a>
            <a class="dropdown-item" href="{{url('/invoice')}}">Invoices</a>
            <a class="dropdown-item" href="{{url('/prepaid_card_settings')}}">Settings</a>
            <a class="dropdown-item" href="{{url('/logout')}}">Logout</a>
          </div>
        </div>
        @endif
      </div>
    </div>
  </div>
</nav>
