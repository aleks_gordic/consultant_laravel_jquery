<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid wraper">
  <a class="navbar-brand" href="{{ route('home') }}"><img src="{{ asset('images/logo.png')}}" alt="logo"/></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
      <div class="end-nav d-flex ml-auto">
        <div class="dropdown">
          <button type="button" class="btn btn-primary nav-right dropdown-toggle user-btn" data-toggle="dropdown">
            <img src="{{asset('images/user-profile.png')}}" alt="icon"><span>{{auth()->user()->first_name}} {{auth()->user()->last_name}}</span>
          </button>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="{{ route('logout')}}">Logout</a>
            <a class="dropdown-item" href="{{ route('settings')}}">My Profile</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</nav>
