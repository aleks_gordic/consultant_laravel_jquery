<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="{{$active =='0'?'active':''}}">
                @if(Auth::user()->role=='customer')
                <a href="{{ route('find_consultant') }}">
                    <i class="fa fa-commenting-o" aria-hidden="true"></i>
                    <span>My Consultants</span>
                </a>
                @elseif(Auth::user()->role=='consultant')
                <a href="{{ route('find_customer') }}">
                    <i class="fa fa-commenting-o" aria-hidden="true"></i>
                    <span>My Customers</span></a>
                @endif
            </li>
            <li class="{{$active =='1'?'active':''}}">
                <a href="{{ route('prepaid_card') }}">
                    <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                    <span>Prepaid Card</span>
                </a>
            </li>
            <li class="{{$active =='2'?'active':''}}">
                <a href="{{ route('invoice') }}">
                    <i class="fa fa-stack-exchange" aria-hidden="true"></i>
                    <span>Invoices</span>
                </a>
            </li>
            <li class="{{$active =='3'?'active':''}}">
                <a href="{{ route('prepaid_card_settings') }}">
                    <i class="fa fa-cogs" aria-hidden="true"></i>
                    <span>Settings</span>
                </a>
            </li>
        </ul>
    </section>
</aside>
