<div class="d-flex flex-column info-footer">
    <div class="d-flex info-footer-sec">
        <div class="foot-start d-flex flex-column">
            <img src="{{ asset('images/logo.png')}}" alt="logo"/>
            <p>Lorem Ipsum er rett og slett dummytekst fra og
            for trykkeindustrien. Lorem Ipsum har vært bransjens 
            standard for dummytekst helt siden 1500-tallet, da en 
            ukjent boktrykker stokket en mengde bokstaver for å lage 
            et prøveeksemplar av en bok.</p>
            <p>Lorem Ipsum har tålt tidens tann!</p>
        </div>
        
        <div class="info-consult d-flex flex-column">
            <label>GoToConsult</label>
            <ul>
                <li><a href="{{ route('about_us') }}">About us</a></li>
                <li><a href="{{ route('become_consultant') }}">Become a Consultant</a></li>
                <li><a href="{{ route('register') }}">Create Account</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
        </div>
        
        <div class="info-cate d-flex flex-column">
            <label>Categories</label>
            <ul>
                @foreach ($categories as $key => $category)
                <?php $route = $category->category_url ?>
                <li><a href="{{url('/category/').'/'.$route}}">{{$category->category_name}}</a></li>
                @endforeach
            </ul>
        </div>
        
        <div class="info-cate d-flex flex-column">
            <label>Get Started</label>
            <ul>
                <li><a href="{{ route('find_consultant') }}">Find a Consultant</a></li>
                <li><a href="#">Become a Consultant</a></li>
                <li><a href="{{ route('login') }}">Login</a></li>
                <li><a href="{{ route('register') }}">Create Account</a></li>
                <li><a href="{{ route('faq') }}">FAQ</a></li>
            </ul>
        </div>
        
        <div class="info-follow d-flex flex-column">
            <label>Follow us</label>
            <ul>
                <li><a href="#">Facebook</a></li>
                <li><a href="#">Instagram</a></li>
                <li><a href="#">LinkedIn</a></li>
            </ul>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="copyright">
            &copy; 2019 Teletjenesten AS. All Rights Reserved 
            <ul>
                <li><a href="{{ route('terms') }}">Terms</a></li>
                <li><a href="{{ route('privacy') }}">Privacy</a></li>
            </ul>
        </div>
        <div class="design">
            UI/UX Design & Full Stack developement: FantasyLab.
        </div>
    </div>
</div>
