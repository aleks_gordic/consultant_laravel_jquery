<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\User;
use App\Models\Categories;
use App\Models\Consultant;
use App\Models\Customer;

class ApiController extends Controller
{   
    public function __construct () {
        if(!Auth::check()){
            return redirect('/home');
        }
    }

    public function updateSetting(Request $request) {
        if ($request->type == 'personal') {
            $rules = array('first_name' => 'required','last_name' => 'required','phone' => 'required|regex:/[0-9]{9}/');
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $response = array(
                    'status' => false,
                    'errors' => $validator->getMessageBag()->toArray()
                );
                return response()->json($response);
            } else {
                $user = Auth::user();
                $user->first_name = $request->first_name;
                $user->last_name = $request->last_name;
                $user->phone = $request->phone;
                $user->save();
                return response()->json(['status' => 'success']);
            }
        } else if ($request->type == 'mail') {
            $rules = array('old_mail' => 'required|email','new_mail' => 'required|email');
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $response = array(
                    'status' => 0,
                    'errors' => $validator->getMessageBag()->toArray()
                );
                return response()->json($response);
            } else {
                if($request->old_mail !=Auth::user()->email) {
                    return response()->json(['status' => 1]);
                } else {
                    if (User::where('email', $request->new_mail)->count() > 0) {
                        return response()->json(['status' => 3]);
                    } else {
                        $user = Auth::user();
                        $user->email = $request->new_mail;
                        $user->save();
                        return response()->json(['status' => 2]);
                    }
                }
            }
        } else {
            $rules = array('old_password' => 'required','new_password' => 'required');
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $response = array(
                    'status' => 0,
                    'errors' => $validator->getMessageBag()->toArray()
                );
                return response()->json($response);
            } else {
                if (Hash::check($request->old_password, Auth::user()->password)) {
                    $user = Auth::user();
                    $user->password = Hash::make($request->new_password);
                    $user->save();
                    return response()->json(['status' => 1]);
                } else {
                    return response()->json(['status' => 2]);
                }
            }
        }
    }

    public function createCategory(Request $request) {
        if($request->type == 'profile') {
            $rules = array('category_name' => 'required|unique:categories', 'category_url' => 'required|unique:categories','category_description' => 'required|max:220');
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $response = array(
                    'status' => false,
                    'errors' => $validator->getMessageBag()->toArray()
                );
                return response()->json($response);
            } else {
                $category = new Categories;
                $category->category_name = $request->category_name;
                $category->category_url = strtolower(str_replace(" ", "_", $request->category_url));
                $category->category_description = $request->category_description;;
                $category->save();
                return response()->json(['status' => true, 'id' => $category->id]);
            }
        } else if ($request->type == 'meta') {
            $category = Categories::where('id', $request->hidden_id)->first();
            $category->meta_title = $request->meta_title;
            $category->meta_description = $request->meta_description;
            $category->save();
        } else {
            $validation = Validator::make($request->all(), [
                'select_file' => 'required|image|mimes:jpeg,png,jpg,gif|max:256'
            ]);
            if($validation->passes()) {
                $image = $request->file('select_file');
                $new_name = rand() . '.' . $image->getClientOriginalExtension();
                $image->move(public_path('images/categories'), $new_name);
                $category = Categories::where('id', $request->hidden_id)->first();
                $category->category_icon = 'images/categories'.$new_name;
                $category->image_access = $request->checkbox_value;
                $category->save();
                return response()->json([
                    'message'   => 'Image Upload Successfully',
                    'uploaded_image' => '<img src="/images/'.$new_name.'" class="img-thumbnail" width="300" />',
                    'class_name'  => 'alert-success'
                ]);
            } else {
                return response()->json([
                    'message'   => $validation->errors()->all(),
                    'uploaded_image' => '',
                    'class_name'  => 'alert-danger'
                ]);
            }
        }
    }
    public function updateCategory(Request $request) {
        if($request->type == 'profile') {
            $rules = array('category_name' => 'required|unique:categories', 'category_url' => 'required|unique:categories','category_description' => 'required|max:220');
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $response = array(
                    'status' => false,
                    'errors' => $validator->getMessageBag()->toArray()
                );
                return response()->json($response);
            } else {
                $category = Categories::where('id', $request->hidden_id)->first();
                $category->category_name = $request->category_name;
                $category->category_url = strtolower(str_replace(" ", "_", $request->category_url));
                $category->category_description = $request->category_description;;
                $category->save();
                return response()->json(['status' => 'true']);
            }
        } else if ($request->type == 'meta') {
            $category = Categories::where('id', $request->hidden_id)->first();
            $category->meta_title = $request->meta_title;
            $category->meta_description = $request->meta_description;
            $category->save();
        } else {
            $validation = Validator::make($request->all(), [
                'select_file' => 'required|image|mimes:jpeg,png,jpg,gif|max:256'
            ]);
            if($validation->passes()) {
                $image = $request->file('select_file');
                $new_name = rand() . '.' . $image->getClientOriginalExtension();
                $image->move(public_path('images/categories'), $new_name);
                $category = Categories::where('id', $request->hidden_id)->first();
                $category->category_icon = 'images/categories/'.$new_name;
                $category->image_access = $request->checkbox_value;
                $category->save();
                return response()->json([
                    'message'   => 'Image Upload Successfully',
                    'uploaded_image' => '<img src="'.$category->category_icon.'" class="img-thumbnail" width="300" />',
                    'class_name'  => 'alert-success'
                ]);
            } else {
                return response()->json([
                    'message'   => $validation->errors()->all(),
                    'uploaded_image' => '',
                    'class_name'  => 'alert-danger'
                ]);
            }
        }
    }

    public function createPage(Request $request) {
        if ($request->type == "page") {
            $rules = array('page_name' => 'required', 'page_url' => 'required');
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $response = array(
                    'status' => false,
                    'errors' => $validator->getMessageBag()->toArray()
                );
                return response()->json($response);
            } else {
                $page = new Page;
                $page->page_name = $request->page_name;
                $page->page_url = strtolower(str_replace(" ", "_", $request->page_url));
                $page->save();
                return response()->json(['status' => true, 'id' => $page->id]);
            }
        } else if ($request->type == 'meta') {
            $rules = array('meta_title' => 'required|max:55', 'meta_description' => 'required|max:55');
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $response = array(
                    'status' => false,
                    'errors' => $validator->getMessageBag()->toArray()
                );
                return response()->json($response);
            } else {
                $page = Page::where('id', $request->hidden_id)->first();
                $page->meta_title = $request->meta_title;
                $page->meta_description = $request->meta_description;
                $page->save();
                return response()->json(['status' => true]);
            }
        } else {
            $rules = array('page_body' => 'required');
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $response = array(
                    'status' => false,
                    'errors' => $validator->getMessageBag()->toArray()
                );
                return response()->json($response);
            } else {
                $page = Page::where('id', $request->hidden_id)->first();
                $page->page_body = $request->page_body;
                $page->save();
                return response()->json(['status' => true]);
            }
        }
    }
    public function updatePage(Request $request) {
        if ($request->type == "page") {
            $rules = array('page_name' => 'required', 'page_url' => 'required');
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $response = array(
                    'status' => false,
                    'errors' => $validator->getMessageBag()->toArray()
                );
                return response()->json($response);
            } else {
                $page = Page::where('id', $request->hidden_id)->first();
                $page->page_name = $request->page_name;
                $page->page_url = strtolower(str_replace(" ", "_", $request->page_url));
                $page->save();
                return response()->json(['status' => true]);
            }
        } else if ($request->type == 'meta') {
            $rules = array('meta_title' => 'required|max:55', 'meta_description' => 'required|max:55');
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $response = array(
                    'status' => false,
                    'errors' => $validator->getMessageBag()->toArray()
                );
                return response()->json($response);
            } else {
                $page = Page::where('id', $request->hidden_id)->first();
                $page->meta_title = $request->meta_title;
                $page->meta_description = $request->meta_description;
                $page->save();
                return response()->json(['status' => true]);
            }
        } else {
            $rules = array('page_body' => 'required');
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $response = array(
                    'status' => false,
                    'errors' => $validator->getMessageBag()->toArray()
                );
                return response()->json($response);
            } else {
                $page = Page::where('id', $request->hidden_id)->first();
                $page->page_body = $request->page_body;
                $page->save();
                return response()->json(['status' => true]);
            }
        }
    }

    public function createConsultant(Request $request) {
        if ($request->type == "profile") {
            $rules = array('first_name' => 'required', 'last_name' => 'required','email' => 'required|email', 'phone' => 'required|regex:/[0-9]{9}/','industry_expertise' => 'required');
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $response = array(
                    'status' => false,
                    'errors' => $validator->getMessageBag()->toArray()
                );
                return response()->json($response);
            } else {
                $user = new User;
                $user->first_name = $request->first_name;
                $user->last_name = $request->last_name;
                $user->email = $request->email;
                $user->phone = $request->phone;
                $user->role = 'consultant';
                $user->save();
                $consultant = new Consultant;
                $consultant->industry_expertise = $request->industry_expertise;
                $consultant->unique_id = $user->id;
                $consultant->save();
                return response()->json(['status' => 1, 'id' => $user->id]);
            }
        } else if ($request->type == "invoice") {
            $consultant = Consultant::where('unique_id', $request->hidden_id)->first();
            $consultant->company_name = $request->company_name;
            $consultant->invoice_mail = $request->invoice_mail;
            $consultant->invoice_first_name = $request->invoice_first_name;
            $consultant->invoice_last_name = $request->invoice_last_name;
            $consultant->address = $request->address;
            $consultant->zip_code = $request->zip_code;
            $consultant->zip_place = $request->zip_place;
            $consultant->save();
            return response()->json(['status' => 'success']);
        } else if ($request->type == "contact") {
            $consultant = Consultant::where('unique_id', $request->hidden_id)->first();
            $consultant->phone_contact = $request->phone_contact;
            $consultant->chat_contact = $request->chat_contact;
            $consultant->video_contact = $request->video_contact;
            $consultant->save();
            return response()->json(['status' => 'success']);
        } else if ($request->type == "password") {
            $consultant = User::where('id', $request->hidden_id)->first();
            if (Hash::check($request->old_password, $consultant->password)) {
                $consultant->password = Hash::make($request->confirm_password);
                $consultant->save();
                return response()->json(['status' => 'success']);
            } else {
                return response()->json(['status' => 1]);
            }
        } else {
            $validation = Validator::make($request->all(), [
                'select_file' => 'required|image|mimes:jpeg,png,jpg,gif|max:256'
            ]);
            if($validation->passes()) {
                $image = $request->file('select_file');
                $new_name = rand() . '.' . $image->getClientOriginalExtension();
                $image->move(public_path('images/consultant'), $new_name);
                $consultant = Consultant::where('id', $request->hidden_id)->first();
                $consultant->prof_image = 'images/consultant'.$new_name;
                $consultant->image_access = $request->checkbox_value;
                $consultant->save();
                return response()->json([
                    'message'   => 'Image Upload Successfully',
                    'uploaded_image' => '<img src="/images/'.$new_name.'" class="img-thumbnail" width="300" />',
                    'class_name'  => 'alert-success'
                ]);
            } else {
                return response()->json([
                    'message'   => $validation->errors()->all(),
                    'uploaded_image' => '',
                    'class_name'  => 'alert-danger'
                ]);
            }
        }
    }
    public function updateConsultant(Request $request) {
        if ($request->type == "profile") {
            $rules = array('first_name' => 'required', 'last_name' => 'required','email' => 'required|email', 'phone' => 'required|regex:/[0-9]{9}/','industry_expertise' => 'required');
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $response = array(
                    'status' => false,
                    'errors' => $validator->getMessageBag()->toArray()
                );
                return response()->json($response);
            } else {
                $user = User::where('id', $request->hidden_id)->first();
                $user->first_name = $request->first_name;
                $user->last_name = $request->last_name;
                $user->email = $request->email;
                $user->phone = $request->phone;
                $user->save();
                $consultant = Consultant::where('unique_id', $request->hidden_id)->first();
                $consultant->industry_expertise = $request->industry_expertise;
                $consultant->save();
                return response()->json(['status' => true]);
            }
        } else if ($request->type == "invoice") {
            $consultant = Consultant::where('unique_id', $request->hidden_id)->first();
            $consultant->company_name = $request->company_name;
            $consultant->invoice_mail = $request->invoice_mail;
            $consultant->invoice_first_name = $request->invoice_first_name;
            $consultant->invoice_last_name = $request->invoice_last_name;
            $consultant->address = $request->address;
            $consultant->zip_code = $request->zip_code;
            $consultant->zip_place = $request->zip_place;
            $consultant->save();
            return response()->json(['status' => 'success']);
        } else if ($request->type == "contact") {
            $consultant = Consultant::where('unique_id', $request->hidden_id)->first();
            $consultant->phone_contact = $request->phone_contact;
            $consultant->chat_contact = $request->chat_contact;
            $consultant->video_contact = $request->video_contact;
            $consultant->save();
            return response()->json(['status' => 'success']);
        } else if ($request->type == "password") {
            $consultant = User::where('id', $request->hidden_id)->first();
            if (Hash::check($request->old_password, $consultant->password)) {
                $consultant->password = Hash::make($request->confirm_password);
                $consultant->save();
                return response()->json(['status' => 'success']);
            } else {
                return response()->json(['status' => 1]);
            }
        } else {
            $validation = Validator::make($request->all(), [
                'select_file' => 'required|image|mimes:jpeg,png,jpg,gif|max:256'
            ]);
            if($validation->passes()) {
                $image = $request->file('select_file');
                $new_name = rand() . '.' . $image->getClientOriginalExtension();
                $image->move(public_path('images/consultant'), $new_name);
                $consultant = Consultant::where('id', $request->hidden_id)->first();
                $consultant->prof_image = 'images/consultant'.$new_name;
                $consultant->image_access = $request->checkbox_value;
                $consultant->save();
                return response()->json([
                    'message'   => 'Image Upload Successfully',
                    'uploaded_image' => '<img src="/images/'.$new_name.'" class="img-thumbnail" width="300" />',
                    'class_name'  => 'alert-success'
                ]);
            } else {
                return response()->json([
                    'message'   => $validation->errors()->all(),
                    'uploaded_image' => '',
                    'class_name'  => 'alert-danger'
                ]);
            }
        }
    }

    public function createCustomer(Request $request) {
        if ($request->type == "profile") {
            $rules = array('first_name' => 'required', 'last_name' => 'required','email' => 'required|email', 'phone' => 'required|regex:/[0-9]{9}/','industry_expertise' => 'required');
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $response = array(
                    'status' => false,
                    'errors' => $validator->getMessageBag()->toArray()
                );
                return response()->json($response);
            } else {
                $user = new User;
                $user->first_name = $request->first_name;
                $user->last_name = $request->last_name;
                $user->email = $request->email;
                $user->phone = $request->phone;
                $user->role = 'customer';
                $user->save();
                $customer = new Customer;
                $customer->industry_expertise = $request->industry_expertise;
                $customer->unique_id = $user->id;
                $customer->save();
                return response()->json(['status' => 1, 'id' => $user->id]);
            }
        } else if ($request->type == "invoice") {
            $customer = Customer::where('unique_id', $request->hidden_id)->first();
            $customer->company_name = $request->company_name;
            $customer->invoice_mail = $request->invoice_mail;
            $customer->invoice_first_name = $request->invoice_first_name;
            $customer->invoice_last_name = $request->invoice_last_name;
            $customer->address = $request->address;
            $customer->zip_code = $request->zip_code;
            $customer->zip_place = $request->zip_place;
            $customer->save();
            return response()->json(['status' => 'success']);
        } else if ($request->type == "contact") {
            $customer = Customer::where('unique_id', $request->hidden_id)->first();
            $customer->phone_contact = $request->phone_contact;
            $customer->chat_contact = $request->chat_contact;
            $customer->video_contact = $request->video_contact;
            $customer->save();
            return response()->json(['status' => 'success']);
        } else if ($request->type == "password") {
            $customer = User::where('id', $request->hidden_id)->first();
            if (Hash::check($request->old_password, $customer->password)) {
                $customer->password = Hash::make($request->confirm_password);
                $customer->save();
                return response()->json(['status' => 'success']);
            } else {
                return response()->json(['status' => 1]);
            }
        } else {
            $validation = Validator::make($request->all(), [
                'select_file' => 'required|image|mimes:jpeg,png,jpg,gif|max:256'
            ]);
            if($validation->passes()) {
                $image = $request->file('select_file');
                $new_name = rand() . '.' . $image->getClientOriginalExtension();
                $image->move(public_path('images/customer'), $new_name);
                $customer = Customer::where('id', $request->hidden_id)->first();
                $customer->prof_image = 'images/customer'.$new_name;
                $customer->image_access = $request->checkbox_value;
                $customer->save();
                return response()->json([
                    'message'   => 'Image Upload Successfully',
                    'uploaded_image' => '<img src="/images/'.$new_name.'" class="img-thumbnail" width="300" />',
                    'class_name'  => 'alert-success'
                ]);
            } else {
                return response()->json([
                    'message'   => $validation->errors()->all(),
                    'uploaded_image' => '',
                    'class_name'  => 'alert-danger'
                ]);
            }
        }
    }
    public function updateCustomer(Request $request) {
        if ($request->type == "profile") {
            $rules = array('first_name' => 'required', 'last_name' => 'required','email' => 'required|email', 'phone' => 'required|regex:/[0-9]{9}/','industry_expertise' => 'required');
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $response = array(
                    'status' => false,
                    'errors' => $validator->getMessageBag()->toArray()
                );
                return response()->json($response);
            } else {
                $user = User::where('id', $request->hidden_id)->first();
                $user->first_name = $request->first_name;
                $user->last_name = $request->last_name;
                $user->email = $request->email;
                $user->phone = $request->phone;
                $user->save();
                $customer = Customer::where('unique_id', $request->hidden_id)->first();
                $customer->industry_expertise = $request->industry_expertise;
                $customer->save();
                return response()->json(['status' => true]);
            }
        } else if ($request->type == "invoice") {
            $customer = Customer::where('unique_id', $request->hidden_id)->first();
            $customer->company_name = $request->company_name;
            $customer->invoice_mail = $request->invoice_mail;
            $customer->invoice_first_name = $request->invoice_first_name;
            $customer->invoice_last_name = $request->invoice_last_name;
            $customer->address = $request->address;
            $customer->zip_code = $request->zip_code;
            $customer->zip_place = $request->zip_place;
            $customer->save();
            return response()->json(['status' => 'success']);
        } else if ($request->type == "contact") {
            $customer = Customer::where('unique_id', $request->hidden_id)->first();
            $customer->phone_contact = $request->phone_contact;
            $customer->chat_contact = $request->chat_contact;
            $customer->video_contact = $request->video_contact;
            $customer->save();
            return response()->json(['status' => 'success']);
        } else if ($request->type == "password") {
            $customer = User::where('id', $request->hidden_id)->first();
            if (Hash::check($request->old_password, $customer->password)) {
                $customer->password = Hash::make($request->confirm_password);
                $customer->save();
                return response()->json(['status' => 'success']);
            } else {
                return response()->json(['status' => 1]);
            }
        } else {
            $validation = Validator::make($request->all(), [
                'select_file' => 'required|image|mimes:jpeg,png,jpg,gif|max:256'
            ]);
            if($validation->passes()) {
                $image = $request->file('select_file');
                $new_name = rand() . '.' . $image->getClientOriginalExtension();
                $image->move(public_path('images/customer'), $new_name);
                $customer = Customer::where('id', $request->hidden_id)->first();
                $customer->prof_image = 'images/customer'.$new_name;
                $customer->image_access = $request->checkbox_value;
                $customer->save();
                return response()->json([
                    'message'   => 'Image Upload Successfully',
                    'uploaded_image' => '<img src="/images/'.$new_name.'" class="img-thumbnail" width="300" />',
                    'class_name'  => 'alert-success'
                ]);
            } else {
                return response()->json([
                    'message'   => $validation->errors()->all(),
                    'uploaded_image' => '',
                    'class_name'  => 'alert-danger'
                ]);
            }
        }
    }
}
