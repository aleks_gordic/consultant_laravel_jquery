<?php
namespace App\Http\Controllers\Client;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Categories;

class PagesController extends Controller
{
    public function index () {
        $categories = Categories::all();
        return view('pages.home', compact('categories'), ['title' => 'Home', 'description' => '']);
    }

    public function category_info(Request $request)
    {
        $categories = Categories::all();
        $category = Categories::where('category_url', $request->type)->first();
        return view('pages.category_info',
            compact('category', 'categories'), ['title' => 'CategoryInfo', 'description' => '']
        );
    }

    public function become_consultant () {
        return view('pages.become_consultant', ['title' => 'BecomeConsultant', 'description' => '']);
    }

    public function about_us () {
        return view('pages.about_us', ['title' => 'AboutUS', 'description' => '']);
    }

    public function faq () {
        return view('pages.faq', ['title' => 'FAQ', 'description' => '']);
    }

    public function find_consultant()
    {
        return view('member.customerchat', ['title' => 'LiveChat', 'description' => '', 'active' => '0']);
    }

    public function find_customer()
    {
        return view('member.consultantchat', ['title' => 'LiveChat', 'description' => '', 'active' => '0']);
    }

    public function prepaid_card() {
        return view('member.prepaid_card', ['title' => 'PaymentCard', 'description' => '', 'active' => '1']);
    }

    public function invoice() {
        return view('member.invoice', ['title' => 'Invoice', 'description' => '', 'active' => '2']);
    }

    public function prepaid_card_settings()
    {
        return view('member.prepaid_card_setting', ['title' => 'Profile', 'description' => '', 'active' => '3']);
    }
}
