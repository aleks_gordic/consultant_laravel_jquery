<?php

namespace App\Http\Controllers\Server;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Models\Page;
use App\User;
use App\Models\Categories;
use App\Models\Consultant;
use App\Models\Customer;

class PagesController extends Controller
{
    public function __construct () {
        if(!Auth::check()){
            return redirect('/home');
        }
    }
    //PAGES
    public function pages () {
        $pages = Page::all();
        return view('admin.pages', compact('pages'), ['active' => '3']);
    }
    public function createPage() {
        return view('admin.create_page', ['active' => '3']);
    }
    public function editPage(Request $request) {
        $page = Page::where('id', $request->id)->first();
        return view('admin.edit_page', compact('page'), ['active' => '3']);
    }
    //CATEGORIES
    public function categories () {
        $categories = Categories::all();
        return view('admin.categories', compact('categories'), ['active' => '2']);
    }
    public function createCategory () {
        return view('admin.create_category', ['active' => '2']);
    }
    public function editCategory (Request $request) {
        $category = Categories::where('id', $request->id)->first();
        return view('admin.edit_category', compact('category'), ['active' => '2']);
    }
    //CUSTOMERS
    public function customers () {
        $customers = User::where('role', 'customer')->get();
        return view('admin.customers', compact('customers'), ['active' => '0']);
    }
    public function createCustomer () {
        $categories = Categories::all();
        return view('admin.create_customer', compact('categories'), ['active' => '0']);
    }
    public function editCustomer (Request $request) {
        $user = User::where('id', $request->id)->first();
        $customer = Customer::where('unique_id', $request->id)->first();
        $categories = Categories::all();
        return view('admin.edit_customer', compact('customer', 'user', 'categories'), ['active' => '0']);
    }
    //CONSULTANTS
    public function consultants () {
        $consultants = User::where('role', 'consultant')->get();
        $info = Consultant::get();
        return view('admin.consultants', compact('consultants', 'info'), ['active' => '1']);
    }
    public function createConsultant () {
        $categories = Categories::all();
        return view('admin.create_consultant', compact('categories'), ['active' => '1']);
    }
    public function editConsultant (Request $request) {
        $user = User::where('id', $request->id)->first();
        $consultant = Consultant::where('unique_id', $request->id)->first();
        $categories = Categories::all();
        return view('admin.edit_consultant', compact('consultant', 'user', 'categories'), ['active' => '1']);
    }
    //SETTING
    public function settting () {
        return view('admin.settings', ['active' => '4']);
    }
}
