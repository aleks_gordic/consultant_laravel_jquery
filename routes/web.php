<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web']], function() {
    Route::get('/', 'Client\PagesController@index')->name('home');
    Route::get('/become_consultant', 'Client\PagesController@become_consultant')->name('become_consultant');
    Route::get('/about_us', 'Client\PagesController@about_us')->name('about_us');
    Route::get('/faq', 'Client\PagesController@faq')->name('faq');
    Route::get('/privacy', 'Client\PagesController@privacy')->name('privacy');
    Route::get('/terms', 'Client\PagesController@terms')->name('terms');
    Route::get('/category/{type}', 'Client\PagesController@category_info')->name('category_info');
    

    //authentication routers
    Route::get('register', ['as' => 'register', 'uses' => 'Auth\RegisterController@showRegistrationForm']);
    Route::post('register', ['as' => 'register', 'uses' => 'Auth\RegisterController@register']);
    Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
    Route::post('login', ['as' => 'login.post', 'uses' => 'Auth\LoginController@login']);
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');

    //admin routers
    Route::get('/pagelist', 'Server\PagesController@pages')->name('pagelist');
    Route::get('/create_page', 'Server\PagesController@createPage')->name('create_page');
    Route::get('/edit_page/{id}', 'Server\PagesController@editPage')->name('edit_page');

    Route::get('/customerlist', 'Server\PagesController@customers')->name('customerlist');
    Route::get('/create_customer', 'Server\PagesController@createCustomer')->name('create_customer');
    Route::get('/edit_customer/{id}', 'Server\PagesController@editCustomer')->name('edit_customer');

    Route::get('/consultantlist', 'Server\PagesController@consultants')->name('consultantlist');
    Route::get('/create_consultant', 'Server\PagesController@createConsultant')->name('create_consultant');
    Route::get('/edit_consultant/{id}', 'Server\PagesController@editConsultant')->name('edit_consultant');

    Route::get('/categorylist', 'Server\PagesController@categories')->name('categorylist');
    Route::get('/create_category', 'Server\PagesController@createCategory')->name('create_category');
    Route::get('/edit_category/{id}', 'Server\PagesController@editCategory')->name('edit_category');

    Route::get('/settings', 'Server\PagesController@settting')->name('settings');

    //member routers
    Route::get('/find_consultant', 'Client\PagesController@find_consultant')->name('find_consultant');
    Route::get('/find_customer', 'Client\PagesController@find_customer')->name('find_customer');
    Route::get('/prepaid_card', 'Client\PagesController@prepaid_card')->name('prepaid_card');
    Route::get('/invoice', 'Client\PagesController@invoice')->name('invoice');
    Route::get('/prepaid_card_settings', 'Client\PagesController@prepaid_card_settings')->name('prepaid_card_settings');

    //API routes
    Route::post('/update_setting', 'Api\ApiController@updateSetting');
    Route::post('/update_category', 'Api\ApiController@updateCategory');
    Route::post('/update_page', 'Api\ApiController@updatePage');
    Route::post('/update_consultant', 'Api\ApiController@updateConsultant');

    Route::post('/create_customer', 'Api\ApiController@createCustomer');
    Route::post('/update_customer', 'Api\ApiController@updateCustomer');
});